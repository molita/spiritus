﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Spiritus.GUI;
using Nuclex.Fonts;
using Nuclex.Graphics;

namespace Spiritus
{
    public class UserInterface
    {
        private SpriteFont spriteFont;
        private List<Window> listOfWindows;
        private List<Button> listOfButtons;
        private bool finishedLoadingContent = false;
        private bool finishedUnloadingContent = false;
        private bool loadOnce = false;
        private VectorFont arialVectorFont;

        private Text helloWorldText;
        private TextBatch textBatch;
        private string dialogTextString = "";
        string[] dialogWrappedString;

        // This is the line height for the text
        private int lineHeight = 28;
        // Line Length for Dialog
        private int lineLength = 55;

        private Button currentSelectedButton;

        public Button CurrentSelectedButton
        {
            set { currentSelectedButton = value; }
            get { return currentSelectedButton; }
        }

        public bool Loaded
        {
            set { finishedLoadingContent = value; }
            get { return finishedLoadingContent; }
        }

        public UserInterface()
        {
            listOfWindows = new List<Window>();
            listOfButtons = new List<Button>();

            textBatch = new TextBatch(GameManager.graphicsDevice);
        }

        public void LoadGUIContent(ContentManager c)
        {
            if (!loadOnce)
            {
                this.arialVectorFont = GlobalVariables.arialVectorFont;
                this.spriteFont = c.Load<SpriteFont>("Fonts/DialogFont");
                this.helloWorldText = this.arialVectorFont.Extrude("Haruhi");
                loadOnce = true;
            }

            switch (GameState.currentGameState)
            {
                case GameStates.MainMenu: // Main Menu
                    finishedLoadingContent = true;
                    break;
                case GameStates.World: // World
                    if (GlobalVariables.interacting)
                    {
                        UnloadGUIContent();
                        CreateParentWindow("bottomDialogBox", GameManager.textureManager.getTexture("GUI/DialogBox", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/DialogBox", 2).Bounds.Width) / 2, GameManager.graphicsDevice.Viewport.Height - GameManager.textureManager.getTexture("GUI/DialogBox", 2).Bounds.Height));
                        CreateParentWindow("characterPortrait", GameManager.textureManager.getTexture(GlobalVariables.interactedCharacter.CharacterPortrait, 2), new Vector2(((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture(GlobalVariables.interactedCharacter.CharacterPortrait, 2).Bounds.Width) / 2) + 400, (GameManager.graphicsDevice.Viewport.Height - GameManager.textureManager.getTexture(GlobalVariables.interactedCharacter.CharacterPortrait, 2).Bounds.Height) / 2));
                        
                        HideWindow("bottomDialogBox");
                        HideWindow("characterPortrait");
                        finishedLoadingContent = true;
                    }
                    break;
                case GameStates.Dungeon: // Dungeon
                    
                    break;
                case GameStates.Battle: // Battle
                    UnloadGUIContent();
                    // Main Bottom Window
                    CreateParentWindow("battleBottomWindow", GameManager.textureManager.getTexture("GUI/BottomBattleMenu", 2), new Vector2(0, GameManager.graphicsDevice.Viewport.Bounds.Height - GameManager.textureManager.getTexture("GUI/BottomBattleMenu", 2).Height));
                    listOfWindows[0].addButton(new Button("button1", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 5), GameManager.graphicsDevice, "Attack"));
                    listOfWindows[0].addButton(new Button("button2", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 65), GameManager.graphicsDevice, "Magic"));
                    listOfWindows[0].addButton(new Button("button3", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 125), GameManager.graphicsDevice, "Item"));
                    listOfWindows[0].addButton(new Button("button4", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 185), GameManager.graphicsDevice, "Run!"));

                    // Bottom Window for Stats
                    CreateParentWindow("battleBottomMainWindow", GameManager.textureManager.getTexture("GUI/800x300BattleMenu", 2), 
                        new Vector2(
                            GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/800x300BattleMenu", 2).Width,
                            GameManager.graphicsDevice.Viewport.Bounds.Height - GameManager.textureManager.getTexture("GUI/800x300BattleMenu", 2).Height));
                    

                    // Button Mappings
                    listOfWindows[0].getButton("button1").downButton = listOfWindows[0].getButton("button2");
                    listOfWindows[0].getButton("button2").downButton = listOfWindows[0].getButton("button3");
                    listOfWindows[0].getButton("button3").downButton = listOfWindows[0].getButton("button4");
                    listOfWindows[0].getButton("button2").upButton = listOfWindows[0].getButton("button1");
                    listOfWindows[0].getButton("button3").upButton = listOfWindows[0].getButton("button2");
                    listOfWindows[0].getButton("button4").upButton = listOfWindows[0].getButton("button3");
                    // Set the first button to be selected
                    currentSelectedButton = listOfWindows[0].getButton("button1");
                    listOfWindows[0].getButton("button1").Selected = true;

                    HideWindow("battleBottomWindow");
                    HideWindow("battleBottomMainWindow");
                    finishedLoadingContent = true;
                    break;
                case GameStates.Cutscene: // Cutscene?
                    break;
                case GameStates.WorldPauseMenu: // World / Dungeon / Pause menu
                    UnloadGUIContent();
                    // First Window - Right Menu
                    CreateParentWindow("rightMenu", GameManager.textureManager.getTexture("GUI/BlueBox200x500", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/BlueBox200x500", 2).Bounds.Width) - 10, 10));
                    listOfWindows[0].addButton(new Button("button1", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 20), GameManager.graphicsDevice, "Button 1"));
                    listOfWindows[0].addButton(new Button("button2", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 90), GameManager.graphicsDevice, "Button 2"));
                    listOfWindows[0].addButton(new Button("button3", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 160), GameManager.graphicsDevice, "Button 3"));
                    listOfWindows[0].addButton(new Button("button4", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 230), GameManager.graphicsDevice, "Button 4"));
                    listOfWindows[0].addButton(new Button("worldPauseMenuExitButton", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 300), GameManager.graphicsDevice, "Exit"));

                    // Button Mappings
                    listOfWindows[0].getButton("button1").downButton = listOfWindows[0].getButton("button2");
                    listOfWindows[0].getButton("button2").downButton = listOfWindows[0].getButton("button3");
                    listOfWindows[0].getButton("button3").downButton = listOfWindows[0].getButton("button4");
                    listOfWindows[0].getButton("button4").downButton = listOfWindows[0].getButton("worldPauseMenuExitButton");
                    listOfWindows[0].getButton("button2").upButton = listOfWindows[0].getButton("button1");
                    listOfWindows[0].getButton("button3").upButton = listOfWindows[0].getButton("button2");
                    listOfWindows[0].getButton("button4").upButton = listOfWindows[0].getButton("button3");
                    listOfWindows[0].getButton("worldPauseMenuExitButton").upButton = listOfWindows[0].getButton("button4");

                    // Set the first button to be selected
                    currentSelectedButton = listOfWindows[0].getButton("button1");
                    listOfWindows[0].getButton("button1").Selected = true;

                    // Sub menus
                    CreateParentWindow("button1Menu", GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2).Bounds.Width) - 240, 20));
                    CreateParentWindow("button2Menu", GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2).Bounds.Width) - 240, 90));
                    CreateParentWindow("button3Menu", GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2).Bounds.Width) - 240, 160));
                    CreateParentWindow("button4Menu", GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2), new Vector2((GameManager.graphicsDevice.Viewport.Width - GameManager.textureManager.getTexture("GUI/BlueBox800x600", 2).Bounds.Width) - 240, 230));
                    // Labels for each Window
                    listOfWindows[1].addLabel(new Label("button1Label", null, new Vector2(20, 20), GameManager.graphicsDevice, "Button 1 Window"));
                    listOfWindows[2].addLabel(new Label("button2Label", null, new Vector2(20, 20), GameManager.graphicsDevice, "Button 2 Window"));
                    listOfWindows[3].addLabel(new Label("button3Label", null, new Vector2(20, 20), GameManager.graphicsDevice, "Button 3 Window"));
                    listOfWindows[4].addLabel(new Label("button4Label", null, new Vector2(20, 20), GameManager.graphicsDevice, "Button 4 Window"));
                    // Buttons for 4th submenu
                    listOfWindows[4].addButton(new Button("window4Button1", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(20, 530), GameManager.graphicsDevice, "Button 1"));
                    listOfWindows[4].addButton(new Button("window4Button2", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(130, 530), GameManager.graphicsDevice, "Button 2"));
                    listOfWindows[4].addButton(new Button("window4Button3", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(240, 530), GameManager.graphicsDevice, "Button 3"));
                    listOfWindows[4].addButton(new Button("window4BackButton", GameManager.textureManager.getTexture("GUI/BlueButton100x50", 2), new Vector2(350, 530), GameManager.graphicsDevice, "Back"));
                    // Button mapping for 4th window
                    listOfWindows[4].getButton("window4Button1").rightButton = listOfWindows[4].getButton("window4Button2");
                    listOfWindows[4].getButton("window4Button2").rightButton = listOfWindows[4].getButton("window4Button3");
                    listOfWindows[4].getButton("window4Button3").rightButton = listOfWindows[4].getButton("window4BackButton");
                    listOfWindows[4].getButton("window4Button3").leftButton = listOfWindows[4].getButton("window4Button2");
                    listOfWindows[4].getButton("window4Button2").leftButton = listOfWindows[4].getButton("window4Button1");
                    listOfWindows[4].getButton("window4BackButton").leftButton = listOfWindows[4].getButton("window4Button3");
                    // Hides all submenus
                    HideWindow("button1Menu");
                    HideWindow("button2Menu");
                    HideWindow("button3Menu");
                    HideWindow("button4Menu");
                    finishedLoadingContent = true;
                    break;
                case GameStates.BattlePauseMenu: // Battle Pause Menu
                    
                    break;
            }
        }
        public Button getButton(string buttonName, string windowName)
        {
            foreach (Window w in listOfWindows)
            {
                if (w.getWindowName == windowName)
                {
                    return w.getButton(buttonName);
                    
                }
            }
            Console.WriteLine("ERROR! Could not find BUTTON: " + buttonName + " from WINDOW: " + windowName);
            return null;
        }
        public void UnloadGUIContent()
        {
            listOfWindows.RemoveRange(0, listOfWindows.Count);
            listOfButtons.RemoveRange(0, listOfWindows.Count);
            finishedUnloadingContent = true;
        }

        private void CreateButton(string name, Texture2D texture, Vector2 position, string label, Window parentWindow)
        {
            parentWindow.addButton(new Button(name, texture, position, GameManager.graphicsDevice, label));
        }

        private void CreateParentWindow(string name, Texture2D texture, Vector2 position)
        {
            listOfWindows.Add(new Window(name, texture, position, GameManager.graphicsDevice));
        }

        private void InsertWindow(Window window, int index)
        {
            if (listOfWindows.Count == index + 1)
                listOfWindows.Add(window);
            else
                listOfWindows.Insert(index + 1, window);
        }

        public void HideWindow(string windowName)
        {
            foreach (Window w in listOfWindows)
            {
                if (w.getWindowName == windowName)
                {
                    w.HideWindow();
                    break;
                }
            }
        }

        public void ShowWindow(string windowName)
        {
            foreach (Window w in listOfWindows)
            {
                if (w.getWindowName == windowName)
                {
                    w.ShowWindow();
                    break;
                }
            }
        }

        private void CreateChildWindow(string parentName, Window childWindow)
        {
            // IMPORTANT: This function assumes that there every element in the List has a unique name (as it should be)
            Window parentWindow = null;
            foreach (Window w in listOfWindows)
            {
                if (w.getWindowName == parentName)
                {
                    parentWindow = w;
                    break;
                }
            }

            if (parentWindow != null)
            {
                InsertWindow(childWindow, listOfWindows.IndexOf(parentWindow));
                parentWindow.addWindow(childWindow);
            }
            else
                Console.WriteLine("Warning: Parent Window is null!");
        }

        private void drawNpcPopup()
        {
            if (GlobalVariables.interactedCharacter != null)
            {
                this.helloWorldText = this.arialVectorFont.Extrude(GlobalVariables.interactedCharacter.getDisplayName);

                this.textBatch.ViewProjection = Camera.ActiveCamera.View * Camera.ActiveCamera.Projection;
                this.textBatch.Begin();
                Matrix newMatrix = Matrix.CreateTranslation(
                    GlobalVariables.interactedCharacter.objectBillboard.Translation 
                    + new Vector3(0, GlobalVariables.interactedCharacter.getBoxHeight + 0.01f, 0));
                Matrix scaleMatrix = Matrix.CreateScale(0.003f);
                Matrix rotatedMatrix = Matrix.CreateRotationY(-Camera.ActiveCamera.Angle.Y);
                Matrix transformedMatrix = scaleMatrix * rotatedMatrix * newMatrix;

                this.textBatch.DrawText(
                  helloWorldText,                  // text mesh to render
                  transformedMatrix,                    // transformation matrix (scale + position)
                  Color.Black                           // text color
                );
                this.textBatch.End();
            }
            
        }

        public void SetDialogText(string text)
        {
            dialogTextString = text;
            dialogWrappedString = Functions.WrapText(dialogTextString, lineLength);
        }

        private void drawDialogText()
        {
            // This is to draw the Dialog Text (text inside the dialog box when characters speak

            GlobalVariables.UserInterfaceSpriteBatch.Begin(
                SpriteSortMode.BackToFront, 
                GlobalVariables.defaultBlendState, 
                SamplerState.AnisotropicWrap, 
                DepthStencilState.DepthRead, 
                GlobalVariables.defaultRasterizer);
            
            // The bounds for the Dialog Box are hardcoded for now
            // Check if the player is currently interacting with an NPC.
            if (GlobalVariables.interacting)
            {
                for (int i = 0; i < dialogWrappedString.Length; i++)
                {
                    if (dialogWrappedString[i] != null)
                        GlobalVariables.UserInterfaceSpriteBatch.DrawString(spriteFont, dialogWrappedString[i], new Vector2(420, 660 + lineHeight * i), Color.Red);
                }
            }
            GlobalVariables.UserInterfaceSpriteBatch.End();
        }

        public void Update(GameTime gametime)
        {
            // For Battle
            if (BattleManager.readyForUI)
            {
                ShowWindow("battleBottomWindow");
                ShowWindow("battleBottomMainWindow");
            }
        }

        public void DrawBattleBottomMenuStats(List<BattleCharacter> listOfCharacters)
        {
            GlobalVariables.UserInterfaceSpriteBatch.Begin(
                SpriteSortMode.BackToFront,
                GlobalVariables.defaultBlendState,
                SamplerState.AnisotropicWrap,
                DepthStencilState.DepthRead,
                GlobalVariables.defaultRasterizer);

            // Draw the names
            GlobalVariables.UserInterfaceSpriteBatch.DrawString(spriteFont, listOfCharacters[0].getDisplayName, new Vector2(820, 620), Color.Red);

            // Draw Health Bars
            GlobalVariables.UserInterfaceSpriteBatch.Draw(
                GameManager.textureManager.getTexture("GUI/DarkGreyHealthBar", 2),
                new Rectangle(1100, 630, 100, 20),
                Color.White);
            GlobalVariables.UserInterfaceSpriteBatch.Draw(
                GameManager.textureManager.getTexture("GUI/GreenHealthBar", 2),
                new Rectangle(1100, 630, (int)(listOfCharacters[0].getHitPointsPercent * 100), 20),
                Color.White);
            
            

            // Draw Action Bars
            GlobalVariables.UserInterfaceSpriteBatch.Draw(
                GameManager.textureManager.getTexture("GUI/DarkGreyHealthBar", 2),
                new Rectangle(1300, 630, 100, 20),
                Color.White);
            GlobalVariables.UserInterfaceSpriteBatch.Draw(
                GameManager.textureManager.getTexture("GUI/RedActionBar", 2),
                new Rectangle(1300, 630, (int)(listOfCharacters[0].getActionBarPercent * 100), 20),
                Color.White);
            

            GlobalVariables.UserInterfaceSpriteBatch.End();
        }


        public void Draw(GameTime gameTime)
        {
            drawNpcPopup();
            foreach (Window w in listOfWindows)
            {
                if (w.isVisible == true)
                {
                    w.DrawElement(gameTime);
                    w.drawWindowChildren(gameTime);
                }
            }
            
            if (spriteFont != null)
                drawDialogText();
        }
    }
}

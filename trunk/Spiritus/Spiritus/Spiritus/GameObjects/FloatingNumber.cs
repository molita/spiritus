﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using Nuclex.Fonts;

namespace Spiritus
{
    public class FloatingNumber
    {
        private float number;
        private Color color;

        private Text floatingNumberText;
        private TextBatch textBatch;

        private bool startedDrawing = false;

        private BattleCharacter battleCharacter;      // Character to draw floating number on
        private double floatingNumberTime;            // Keep track of how long the floating number has been up

        public FloatingNumber(float n, Color c, BattleCharacter character)
        {
            number = n;
            color = c;
            battleCharacter = character;

            string numberString = n.ToString();     // Convert number to a string
            floatingNumberText = GlobalVariables.arialVectorFont.Extrude(numberString);    // Create the number Text

        }

        public bool DrawFloatingNumber(GameTime gameTime)
        {
            if (startedDrawing)
            {
                floatingNumberTime += gameTime.ElapsedGameTime.TotalMilliseconds;

                if (Math.Abs(floatingNumberTime) < 1000)
                {
                    textBatch.ViewProjection = Camera.ActiveCamera.View * Camera.ActiveCamera.Projection;
                    textBatch.Begin();
                    Matrix scaleMatrix = Matrix.CreateScale(0.007f);
                    Matrix rotationMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(180));
                    Matrix testMatrix = Matrix.CreateBillboard(
                        battleCharacter.Location + new Vector3(-battleCharacter.getBoxWidth + 0.03f, battleCharacter.getBoxHeight + (float)(floatingNumberTime * 0.0001), 0),
                        Camera.ActiveCamera.Position,
                        Camera.ActiveCamera.getUpVector,
                        Camera.ActiveCamera.getForwardVector);

                    Matrix transformedMatrix = scaleMatrix * rotationMatrix * testMatrix;

                    if (GlobalVariables.DEBUG)
                    {
                        Console.WriteLine("Battle Character Position: " + battleCharacter.Location);
                    }
                    textBatch.DrawText(
                        floatingNumberText,                   // text mesh to render
                        transformedMatrix,                    // transformation matrix (scale + position)
                        color                                 // text color
                    );
                    textBatch.End();
                    return true;
                }
                else
                    return false;
            }
            else
            {
                textBatch = new TextBatch(GameManager.graphicsDevice);
                startedDrawing = true;

                return true;
            }
        }
    }
}

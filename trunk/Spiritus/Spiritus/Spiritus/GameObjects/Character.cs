﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using DPSF.ParticleSystems;
using Spiritus.GameObjects;

namespace Spiritus
{
    public class Character : WorldObject, IComparable, GlobalVariables.ListOfSprites
    {
        private string characterDisplayName;
        private string characterNameSingleWord;
        private Texture2D character;
        private string characterPortrait;
        private Vector3 drawPosition;

        // Character 3D Bounding Sphere
        private BoundingSphere characterBoundingSphere;

        // Character Animation Variables
        private Texture2D currentAnimationFrame;
        // Different Animation Sets
        private Texture2D[] characterStandAnimations;
        private Texture2D[] characterWalkAnimations;
        private Texture2D[] characterBattleStandAnimations;

        private int currentAnimationState = 1;
        private double tempGameTime;
        private double animationInterval = 200;
        private int animationCounter1 = 0;
        private int animationCounter2 = 0;

        private float posX = 1f, posY = 0, posZ = -1f;
        private Vector3 characterDirection = Vector3.Forward;
        private float scale = 1.0f;

        // Some Battle Variables - I'll think about refactoring these properly later
        private bool thisBattleCharacterSelected = false;

        BlendState defaultBlendState;

        // Character Type
        private CharacterType cType;

        // The following variables are required for BattleCharacter Inheritance
        private Texture2D[] animationSets;
        private Vector3[] animationFrames;

        public string CharacterPortrait
        {
            set { characterPortrait = value; }
            get { return characterPortrait; }
        }

        public int animationState
        {
            set { currentAnimationState = value; }
            get { return currentAnimationState; }
        }

        public CharacterType characterType
        {
            set { cType = value; }
            get { return cType; }
        }

        public Vector3 Direction
        {
            set { characterDirection = value; }
            get { return characterDirection; }
        }

        public float Scale
        {
            set { scale = value; }
            get { return scale; }
        }

        // Position Parameters
        public float PositionY
        {
            set { posY = value; }
            get { return base.Location.Y; }
        }
        
        public float PositionX
        {
            set { posX = value; }
            get { return base.Location.X; }
        }

        public float PositionZ
        {
            set { posZ = value; }
            get { return base.Location.Z; }
        }

        public string NameSingleWord
        {
            set { characterNameSingleWord = value; }
            get { return characterNameSingleWord; }
        }

        public string getDisplayName { get { return characterDisplayName; } }
        // The following properties are required for the BattleCharacter Inheritance
        public Texture2D getSingleTexture { get { return character; } }
        public Texture2D[] getAnimationSet { get { return animationSets; } }
        public Vector3[] getAnimationFrameData { get { return animationFrames; } }
        public bool setBattleCharacterSelected { set { thisBattleCharacterSelected = value; } }

        public Character(string n, Texture2D a, Vector2 startPos, Texture2D[] animationSet, Vector3[] animationFrameData, float billboardBoxWidth, float billboardBoxRatio) 
            : base(n, new Vector3(startPos.X, 100, -startPos.Y), 0.12f, billboardBoxWidth, billboardBoxRatio * billboardBoxWidth)
        {
            LoadAnimations(animationSet, animationFrameData);
            this.character = a;
            this.characterDisplayName = n;
            characterNameSingleWord = characterDisplayName.Replace(" ", "");

            // These variables are required for BattleCharacter Inheritance
            animationSets = animationSet;
            animationFrames = animationFrameData;

            posX = startPos.X;
            posZ = startPos.Y;

            defaultBlendState = new BlendState();
            defaultBlendState = BlendState.AlphaBlend;
            GameManager.graphicsDevice.BlendState = defaultBlendState;

            base.Texture = a;
        }

        private void LoadAnimations(Texture2D[] a, Vector3[] b)
        {
            
            int start = 0;  // X
            int count = 0;  // Y
            int state = 0;  // Z
            
            // STATES:
            // State 1: Standing
            // State 2: Walking
            // State 3: Standing (Battle)

            for (int i = 0; i < b.Length; i++)
            {
                start = (int)b[i].X;
                count = (int)b[i].Y;
                state = (int)b[i].Z;

                if (state == 1) characterStandAnimations = new Texture2D[count];
                if (state == 2) characterWalkAnimations = new Texture2D[count];
                if (state == 3) characterBattleStandAnimations = new Texture2D[count];

                for (int j = 0; j < count; j++)
                {
                    if (state == 1)
                        characterStandAnimations[j] = a[start + j];
                    else if (state == 2)
                        characterWalkAnimations[j] = a[start + j];
                    else if (state == 3)
                        characterBattleStandAnimations[j] = a[start + j];
                }
            }

        }

        public void CharacterAnimation(int state, GameTime gameTime)
        {
            tempGameTime += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (tempGameTime > animationInterval)
            {
                if (state == 1)
                {
                    currentAnimationFrame = characterStandAnimations[animationCounter1];
                    if (animationCounter1 + 1 == characterStandAnimations.Length)
                        animationCounter1 = 0;
                    else
                        animationCounter1++;
                }
                else if (state == 2)
                {
                    currentAnimationFrame = characterWalkAnimations[animationCounter2];
                    if (animationCounter2 + 1 == characterWalkAnimations.Length)
                        animationCounter2 = 0;
                    else
                        animationCounter2++;
                }
                else if (state == 3)
                {
                    currentAnimationFrame = characterBattleStandAnimations[animationCounter1];
                    if (animationCounter1 + 1 == characterBattleStandAnimations.Length)
                        animationCounter1 = 0;
                    else
                        animationCounter1++;
                }

                tempGameTime = 0;           // Reset the timer
            }
        }

        public void SetY(float[,] height, Vector3 position)
        {
            float calculatedY = Functions.CalculateY(height, position);
            base.Location = new Vector3(base.Location.X, calculatedY, base.Location.Z);
        }

        public void FlipCharacterOrientation()
        {
        }
        private void UpdateDrawPositions()
        {
            base.Location = new Vector3(posX, base.Location.Y, posZ);
            posY = base.Location.Y;
        }

        public override void Draw(GameTime gameTime)
        {
            // Set the animation frame
            CharacterAnimation(animationState, gameTime);
            // Sets the Y value for the character
            // It should be 0 for Battle.
            // For World it should be from the heightmap

            if (GameState.currentGameState == GameStates.World)
                SetY(GlobalVariables.currentWorld.getHeightData, Location);
            else
                PositionY = 0;

            UpdateDrawPositions();
            base.Texture = currentAnimationFrame;
            base.UpdateBillBoard();
            base.Draw(gameTime);
            
        }
    }
}
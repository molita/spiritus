﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using Microsoft.Xna.Framework.Graphics;

using Spiritus.DataStructures;
using Nuclex.Fonts;
using Nuclex.Graphics;

using System.Timers;

namespace Spiritus
{
    public class BattleCharacter : Character, GlobalVariables.ListOfSprites
    {
        // Character Attributes
        private int attributeAnger = 3;      // Direct Damage Strength
        private int attributeEmpathy = 2;    // Aiding Others
        private int attributeMalice = 2;     // Damage Others while benefitting self
        private int attributeLove = 2;       // Aiding Others while benefitting self
        private int attributeTenacity = 3;   // Buffing oneself
        private int attributeIntimidation = 3;   // Debuffing Enemies
        private int attributePride = 3;      // Consistency with actions

        // Other character parameters
        private float hitPoints = 1000;
        private float maxHitPoints = 1000;

        private float actionSpeed;       // The speed of the character's Action bar regeneration rate
        private float actionBar = 0;           // Current Action Bar Value
        private float actionBarCoefficient = 10;
        private float actionBarReadyValue;

        private float minCoefficient = 5;      // Lowest speed
        private float maxCoefficient = 20;     // Highest speed

        private bool characterReady = false;                // To indicate when this character's action bar is filled

        private bool characterDead = false;
        private BattleGridPosition gridPosition;

        private string currentQueuedCommand;

        #region Parameters

        public double getHitpoints { get { return hitPoints; } }

        public double getMaxHitPoints { get { return getMaxHitPoints; } }

        public double getActionSpeed { get { return actionSpeed; } }

        public double getActionBar { get { return actionBar; } }

        public bool getReadyStatus { get { return characterReady; } }

        public bool isCharacterDead { get { return characterDead; } }

        public BattleGridPosition getGridPosition { get { return gridPosition; } }

        public string getCurrentQueuedCommand { get { return currentQueuedCommand; } }

        public float getActionBarPercent { get { return actionBar / actionBarReadyValue; } }

        public float getHitPointsPercent { get { return hitPoints / maxHitPoints; } }

        #endregion 

        public BattleCharacter(Character c, Vector2 p, BattleGridPosition battlePos, bool isEnemy) : base(
            c.getDisplayName,
            c.getSingleTexture,
            p,
            c.getAnimationSet,
            c.getAnimationFrameData,
            c.getBoxWidth,
            c.getBoxHeight / c.getBoxWidth)
        {
            // TEMP
            // randomize the attributes
            attributeAnger = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributeEmpathy = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributeIntimidation = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributeLove = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributeMalice = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributePride = GlobalVariables.enemyTargetRandomizer.Next(5);
            attributeTenacity = GlobalVariables.enemyTargetRandomizer.Next(5);

            calculateAttributeValues();     // Calculate Battle Character Attributes from equation

            // Check if character is an Enemy or a Player, and set the corresponding CharacterType
            if (isEnemy)
                base.characterType = CharacterType.Enemy;
            else
                base.characterType = CharacterType.Player;

            // Set the Grid Positions
            gridPosition = battlePos;

            Location = new Vector3(p.X, 0, p.Y);    // Set the position of the character

            
        }


        private void calculateAttributeValues()
        {
            // Calculate Character's actionSpeed
            // Arbitrary - cofficients currently add to 10

            // Set the Action Bar Speed
            actionSpeed = 
                (
                3 * attributeAnger +
                1 * attributeEmpathy +
                1 * attributeMalice +
                1 * attributeLove +
                2 * attributeTenacity +
                2 * attributeIntimidation) * attributePride;

            float actionBarReadyValueCoefficient =
                2 * attributeAnger +
                4 * attributeEmpathy +
                4 * attributeMalice +
                4 * attributeLove +
                3 * attributeTenacity +
                3 * attributeIntimidation;
            
            // Set the Max Action Bar value
            actionBarReadyValue = 
                6000 + (
                actionBarReadyValueCoefficient *
                (float)Math.Sqrt(actionBarReadyValueCoefficient)* 
                (float)Math.Sqrt(attributePride));
        }

        public void modifyHitPoints(float value)
        {
            hitPoints = hitPoints + value;
        }

        public void incrementActionBar()
        {
            // Have to think of a formula to increment the action bar

            float incrementValue = (float)Math.Sqrt(actionBarCoefficient * actionSpeed) * (GameManager.delta * 0.06f);

            actionBar = actionBar + incrementValue;

            // Check if character is ready
            if (actionBar >= actionBarReadyValue)
            {
                characterReady = true;
                if (GlobalVariables.DEBUG)
                    Console.WriteLine("CHARACTER " + this.getDisplayName + " is ready!");
            }
        }

        public void resetActionBar()
        {
            actionBar = 0;
            characterReady = false;
        }

        public void takeActionAI(List<BattleCharacter> listOfPlayerCharacters, List<BattleCharacter> listOfEnemyCharacters, List<FloatingNumber> listOfFloatingNumbers, InitiateBattleSpells initiateBattleSpells)
        {
            // Calculate Action Options


            // Choose Target
            ChooseRandomPlayerTarget();
            // Execute Action
            takeActionOnTarget(listOfPlayerCharacters[0], "attack", listOfFloatingNumbers, initiateBattleSpells);

            // Reset Action Bar
            resetActionBar();
            
            // Clear queued command
            currentQueuedCommand = "";
        }

        public void queueNewCommand(string command)
        {
            currentQueuedCommand = command;
        }

        private int ChooseRandomPlayerTarget()
        {
            // Generate a random target number 
            // Note: Since the player will always have 3 characters,
            // This can be hardcoded to 3 targets
            int target = GlobalVariables.enemyTargetRandomizer.Next(0, 3);

            if (GlobalVariables.DEBUG)
                Console.WriteLine("Character " + getDisplayName + " chooses target " + target);

            return target;
        }
        public void ApplySpellDelay(float delay)
        {
            // Calculate the new action bar coefficient
            float newCoefficient = actionBarCoefficient + delay;

            // Check if the new coefficient is below the minimum
            if (newCoefficient < minCoefficient)
                newCoefficient = minCoefficient;    // Set the coefficient to the minimum
            // Check if the new coefficient is above the maximum
            if (newCoefficient > maxCoefficient)
                newCoefficient = maxCoefficient;    // Set the coefficient to the maximum

            actionBarCoefficient = newCoefficient;  // Set the new coefficient
        }
        public void takeActionOnTarget(BattleCharacter targetCharacter, string command, List<FloatingNumber> listOfFloatingNumbers, InitiateBattleSpells initiateBattleSpells)
        {
            switch (command)
            {
                case "attack":
                    // Calculate Damage
                    float damage =
                        (
                        4 * attributeAnger +
                        1 * attributeEmpathy +
                        3 * attributeMalice +
                        1 * attributeLove +
                        2 * attributeTenacity +
                        3 * attributeIntimidation) * (float)Math.Log(attributePride, 2);
                    float roundedDamage = (float)Math.Round(damage);

                    // Add randomizer to damage
                    // Based on Pride
                    float roundedMinRange = (float)-Math.Abs(Math.Round(Math.Sqrt(attributePride)));
                    float roundedMaxRange = -roundedMinRange;

                    float addedDamageRandomizer = GlobalVariables.damageRandomizer.Next((int)roundedMinRange, (int)roundedMaxRange);

                    // Add the damage randomizer to the rounded damage
                    roundedDamage = roundedDamage + addedDamageRandomizer;

                    Random rrandom = new Random();
                    
                    if (rrandom.Next(0, 2) == 0)
                        initiateBattleSpells.CreateFireball(this, targetCharacter, -(roundedDamage + 10));
                    else
                        initiateBattleSpells.CreateIceLance(this, targetCharacter, -roundedDamage);
                    
                    break;

            }
        }

        // Called when the Battle Character is being selected
        public void SelectBattleCharacter()
        {
            setBattleCharacterSelected = true;
            DiffuseObjectColor(0.5f, 0.5f, 0.5f);
        }
        // Called when the Battle Character is no longer being selected
        public void UnselectBattleCharacter()
        {
            setBattleCharacterSelected = false;
            DiffuseObjectColor(1f, 1f, 1f);
        }

    }
}

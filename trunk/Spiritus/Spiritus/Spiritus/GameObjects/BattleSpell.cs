﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Spiritus.DataStructures;

namespace Spiritus
{
    /// <summary>
    /// This is an object that is comprised of the following components:
    ///     - One or more Particle Systems
    ///     - Movement Translation Queue (Basically a/several paths that the "spell" takes)
    ///     - Origin of spell (the BattleCharacter)
    ///     - Target of spell (BattleCharacter, possibly multiple BattleCharacters
    /// </summary>

    public class BattleSpell
    {
        private string name;                                        // Name of the BattleSpell

        private BattleCharacter originBattleCharacter;              // The character from which the spell originates from
        private BattleCharacter[] targetCharacter;                  // The target(s) of the spell
        private MovementTranslation[,] movementTranslationQueue;    // Movement Translation Queue, where the first index 
                                                                    // is the index of the particle in the local variable
                                                                    // particleSystem. This may be 1 or more MovementTranslations
        private ExtendedParticleEffect[] particleSystem;            // The particle System(s). This may be 1 or more.


        private int currentParticleIndex = 0;   // Store the current Particle Index
        private int currentMovementIndex = 0;   // Store the current Movement Index

        private int particleArraySize;          // Store the size of the Particle array
        private bool completedMovement = false; // Flag for when the Spell has completed all movements
        private bool completedCurrentMovement = false;  // Flat for when the current Movement has completed

        private double delayTime = 0;        // For keeping track of the delay

        private float valueDamage = 0;        // For storing the Damage Value of the spell
        private List<FloatingNumber> floatingNumbers;       // to inherit the list of floating numbers from BattleManager

        // One use of this property is to trigger when the damage actually triggers for the spell
        public bool isDoneMovement
        {
            get { return completedMovement; }
        }

        // This is for identifying the Battle Spell when having to remove it from the list
        public string getName
        {
            get { return name; }
        }

        // With the way this is currently written, one movement will occur at a time (no simultaneous movements), and one
        // particle system will move at a time, in the order of the particles array. This would mean that any particles that
        // would want the effect of *pausing* the particles, move another particle system, then resuming that previous particle system
        // would have to create a new particle system in the array.
        // Alternatively, I can write a method that can tell the order of particles to execute movements in, but this could get complicated...

        public BattleSpell(string n, BattleCharacter origin, BattleCharacter[] targets, MovementTranslation[,] movements, ExtendedParticleEffect[] particles, float damage, List<FloatingNumber> listOfFloatingNumbers)
        {
            // Set the input variables
            name = n;
            originBattleCharacter = origin;
            targetCharacter = targets;
            movementTranslationQueue = movements;
            particleSystem = particles;
            valueDamage = damage;
            floatingNumbers = listOfFloatingNumbers;

            // Set the Particle Array Size
            particleArraySize = particles.Length;
        }

       

        private void ExecuteMovementTranslation(int particleIndex, int movementIndex, GameTime gameTime)
        {
            Vector3 startPosition = movementTranslationQueue[particleIndex, movementIndex].getStartPosition;    // Store Start Position
            Vector3 endPosition = movementTranslationQueue[particleIndex, movementIndex].getEndPosition;        // Store End Position
            float speed = movementTranslationQueue[particleIndex, movementIndex].getSpeed;                      // Store Speed

            // Translate the particle in 3D Space
            particleSystem[particleIndex].Translate3DLocation(startPosition, endPosition, speed, gameTime);
        }

        public void Update(GameTime gameTime)
        {
            // Cycle Through Movements and execute

            // Only execute if all movements have not been completed
            if (!completedMovement)
            {
                // Execute movement
                ExecuteMovementTranslation(currentParticleIndex, currentMovementIndex, gameTime);

                // Check if the particle is no longer moving
                // If it's no longer moving, then the particle should be done its current movement
                if (!particleSystem[currentParticleIndex].isParticleMoving)
                {
                    // First check if there's a pause after the current movement
                    if (movementTranslationQueue[currentParticleIndex, currentMovementIndex].getPostDelay > 0)
                    {
                        delayTime += gameTime.ElapsedGameTime.TotalMilliseconds;        // Increment the delayTime

                        // Check the time elapsed from the delay time to current time
                        // If the difference is greater than delayTime, then the pause is done and set the flag
                        if (delayTime >
                            movementTranslationQueue[currentParticleIndex, currentMovementIndex].getPostDelay)
                        {
                            delayTime = 0;                          // Reset delayTime back to 0 for next movement
                            completedCurrentMovement = true;        // Set the completedCurrentMovement flag
                        }
                    }
                    else
                        completedCurrentMovement = true;
                }

                if (completedCurrentMovement)
                {
                    completedCurrentMovement = false;
                    // Increment indices

                    currentMovementIndex++;     // Increment Movement Index

                    // Check if the current movement index is past the number of elements in the Movement array of the current Particle index
                    if (currentMovementIndex >= movementTranslationQueue.GetLength(currentParticleIndex + 1))
                    {
                        particleSystem[currentParticleIndex].FadeOutParticle();
                        currentParticleIndex++;     // Increment the current Particle Index
                    }
                    

                    // Check if the current Particle index is past the number of elements in the Particle array
                    if (currentParticleIndex >= particleSystem.Length)
                    {
                        // This means the Spell has completed executing its movement animations
                        completedMovement = true;       // Set the completed movement flag
                        foreach (BattleCharacter b in targetCharacter)
                        {
                            DrawFloatingNumber(b, valueDamage, floatingNumbers);
                        }
                    }
                }
                
            }

            // Check if the spell is an Ice Lance
            if (this.particleSystem[0].particleSystem.GetType() == GlobalVariables.dummyIceLance.GetType())
            {
                // If it's fading, also fade out the texture
                //if (particleSystem[0].getFadingStatus)
                    //((DPSF.ParticleSystems.P_IceLance)particleSystem[0].particleSystem).setTextureAlpha -= (float)gameTime.ElapsedGameTime.TotalMilliseconds*0.001f;
                ((DPSF.ParticleSystems.P_IceLance)particleSystem[0].particleSystem).UpdateBillboards();
            }
        }

        // Note: The damage is also done through here when the floating number is drawn
        private void DrawFloatingNumber(BattleCharacter target, float value, List<FloatingNumber> listOfFloatingNumbers)
        {
            // Modify Hit Points of the character
            target.modifyHitPoints(value);

            // Set the color to be Red (damage) or Green (heal)
            Color color = new Color();
            if (value < 0)
                color = Color.Red;      // Damage Color
            else
                color = Color.Green;    // HP Healing Color

            // Add floating number to the list
            listOfFloatingNumbers.Add(new FloatingNumber(Math.Abs(value), color, target));
        }
    }
}



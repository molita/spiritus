﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Spiritus.GameObjects
{
    /// <summary>
    /// A World Object is anything that is a physically in the world, apart from the terrain and background (skybox).
    /// These include characters, spells, buildings, random world elements like trees, grass, etc.
    /// A World object will be billboarded.
    /// 
    /// Each World Object will have the following properties:
    /// 
    /// Name: A string that will distinguish the object from other objects. (it's possible to have the same name for certain 
    ///     objects, such as blades of grass. Since those blades of grass will generally be added/removed in batches anyway, 
    ///     we can just make a call to the method to remove all objects of a certain name)
    /// Location: A Vector3 that is the physical location of the object.
    /// Texture: A Texture2D. Optional. This would generally be for static objects (no animation). 
    /// Radius: A float that is the "size" of the object, used for collision. 
    ///     Due to the simplicity of the game, each object will have a spherical collision radius.
    ///     Furthermore, every object's collision sphere origin Y value will be equal to the height
    ///     of the terrain at its location.
    /// 
    /// 
    /// Each World Object will have the following Methods:
    /// 
    /// TranslateLocation: Using the Smoothstep method to translate the location of the object
    /// 
    /// 
    /// </summary>

    public class WorldObject : IComparable, GlobalVariables.ListOfSprites
    {
        private string objectName;          // Name of the Object.
        private Vector3 objectLocation;     // Location of the Object.
        private Texture2D objectTexture;    // Texture of the Object. Only used for static Objects with one texture.
        private float objectRadius;         // The size of the radius of the Object.

        private TranslationType currentTranslationType; // The current translation type
        private Vector3 desiredLocation;                // The desired location to translate to
        private float objectTranslationSpeed;           // The speed of the translation - Value between 0 and 1

        // Object's billboard box variables
        private VertexPositionTexture[] objectBoxVertices;      // Stores the Vertices of the billboard box
        private int[] objectBoxIndices;                         // Stores the Indices of the triangles that make up the billboard box
        public Matrix objectBillboard = new Matrix();           // The billboard Matrix
        private float objectBoxWidth;                           // The width of the box (Default for characters is .15)
        private float objectBoxHeight;                          // The height of the box (Default for characters is .30)   


        private float distanceFromCamera;   // The distance of the object from the camera. This is used to sort the objects in draw order.

        // Variables for the Ray to calculate the Y value at a certain location in the World
        private Ray objectWorldPosition;
        private BoundingSphere objectBoundingSphere;    // Object 3D Bounding Sphere

        private bool objectFlipped = false;     // Boolean to check if object texture is flipped horizontally

        // Effect
        BasicEffect objectBasicEffect;

        // Variables to calculate Camera X Angle
        private Vector3 flatPlaneVector = new Plane(
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            new Vector3(0, 0, 1)).Normal;

        #region Properties
        
        public Vector3 Location
        {
            set { objectLocation = value; }
            get { return objectLocation; }
        }

        public Texture2D Texture
        {
            set { objectTexture = value; }
            get { return objectTexture; }
        }

        public float Radius
        {
            set { objectRadius = value; }
            get { return objectRadius; }
        }


        // Properties that are initialized and never changed outside of this class
        // These will all only have a get method

        public string getName { get { return objectName; } }
        public BoundingSphere getBoundingSphere { get { return objectBoundingSphere; } }
        public float getDistanceFromCamera { get { return distanceFromCamera; } }
        public float getBoxWidth { get { return objectBoxWidth; } }
        public float getBoxHeight { get { return objectBoxHeight; } }

        #endregion

        
        public int CompareTo(object obj)
        {
            WorldObject otherWorldObject = obj as WorldObject;

            // Compare the distance of the object with this object
            return this.distanceFromCamera.CompareTo(otherWorldObject.distanceFromCamera);
        }
        
        // Constructor
        public WorldObject(string name, Vector3 initialLocation, float initialRadius, float boxWidth, float boxHeight)
        {
            // Set properties
            objectName = name;
            objectLocation = initialLocation;
            objectBoundingSphere = new BoundingSphere();
            objectBoundingSphere.Radius = initialRadius;
            objectBoxWidth = boxWidth;
            objectBoxHeight = boxHeight;

            // Allocate Memory for Variables
            objectBasicEffect = new BasicEffect(GameManager.graphicsDevice);

            CreateObjectBox();
            SetUnflippedVertices();

            objectBasicEffect.TextureEnabled = true;

        }
        
        private void CreateObjectBox()
        {
            // For simplicity, each billboard will have exactly 4 vertices, essentially a rectangle.
            int boxVerticesHeight = 2;  // Vertices - min is 2
            int boxVerticesWidth = 2;   // Vertices - min is 2

            objectBoxVertices = new VertexPositionTexture[boxVerticesHeight * boxVerticesWidth];

            SetUnflippedVertices();     // Default is unflipped

            objectBoxIndices = new int[6];

            objectBoxIndices[0] = 3;
            objectBoxIndices[1] = 2;
            objectBoxIndices[2] = 1;
            objectBoxIndices[3] = 2;
            objectBoxIndices[4] = 0;
            objectBoxIndices[5] = 1;
        }

        // This method sets the vertices so that the texture drawn is flipped
        private void SetFlippedVertices()
        {
            objectBoxVertices[0] = new VertexPositionTexture(
                        new Vector3(objectBoxWidth, objectBoxHeight, 0),
                        new Vector2(1, 0));
            objectBoxVertices[2] = new VertexPositionTexture(
                        new Vector3(objectBoxWidth, 0, 0),
                        new Vector2(1, 1));
            objectBoxVertices[1] = new VertexPositionTexture(
                        new Vector3(0, objectBoxHeight, 0),
                        new Vector2(0, 0));
            objectBoxVertices[3] = new VertexPositionTexture(
                        new Vector3(0, 0, 0),
                        new Vector2(0, 1));
        }
        // This method sets the vertices so that the texture drawn is unflipped
        private void SetUnflippedVertices()
        {
            objectBoxVertices[0] = new VertexPositionTexture(
                        new Vector3(objectBoxWidth, objectBoxHeight, 0),
                        new Vector2(0, 0));
            objectBoxVertices[2] = new VertexPositionTexture(
                        new Vector3(objectBoxWidth, 0, 0),
                        new Vector2(0, 1));
            objectBoxVertices[1] = new VertexPositionTexture(
                        new Vector3(0, objectBoxHeight, 0),
                        new Vector2(1, 0));
            objectBoxVertices[3] = new VertexPositionTexture(
                        new Vector3(0, 0, 0),
                        new Vector2(1, 1));
        }
        
        // Update the object billboard based on the Camera
        public void UpdateBillBoard()
        {
            objectBillboard = Matrix.CreateBillboard(
                objectLocation,
                Camera.ActiveCamera.Position,
                Camera.ActiveCamera.getUpVector,
                Camera.ActiveCamera.getForwardVector);
        }
        

        // Interpolate between two Vector3s with a specified method
        // Warning! This function will override any previous input.
        public void TranslateLocation(Vector3 newLocation, float speed, TranslationType type)
        {
            desiredLocation = newLocation;      // Set the new desired location to translate to
            currentTranslationType = type;      // Set the type of translation
            objectTranslationSpeed = speed;
        }

        public void Update(GameTime gameTime)
        {
            // Update the object's location
            switch (currentTranslationType)
            {
                case TranslationType.Linear:
                    objectLocation = Vector3.Lerp(objectLocation, desiredLocation, objectTranslationSpeed);
                    break;
                case TranslationType.Cubic:
                    objectLocation = Vector3.SmoothStep(objectLocation, desiredLocation, objectTranslationSpeed);
                    break;
            }
        }

        // This method diffuses the color of the object. 
        // Basically means it'll darken the color (can't lighten past 1.0, which is the default color)
        public void DiffuseObjectColor(float R, float G, float B)
        {
            objectBasicEffect.DiffuseColor = new Vector3(R, G, B);
        }

        public virtual void Draw(GameTime gameTime)
        {
            UpdateBillBoard();  // Update Billboard

            objectWorldPosition.Position = objectLocation;      // Update the Object's Location
            distanceFromCamera =                                // Update the distance of the object from the Camera
                Vector3.Distance(
                objectWorldPosition.Position, 
                Camera.ActiveCamera.Position);
            objectBoundingSphere.Center =                       // Updating the center of the bounding sphere
                objectWorldPosition.Position; 

            // Set Effect
            objectBasicEffect.World = objectBillboard;                      // Set the World
            objectBasicEffect.View = Camera.ActiveCamera.View;              // Set the View
            objectBasicEffect.Projection = Camera.ActiveCamera.Projection;  // Set the Projection
            objectBasicEffect.Texture = objectTexture;                      // Set the texture

            objectBasicEffect.CurrentTechnique.Passes[0].Apply();           // Apply the technique

            // Draw
            GameManager.graphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
                PrimitiveType.TriangleList,
                objectBoxVertices,
                0,
                objectBoxVertices.Length,
                objectBoxIndices,
                0,
                objectBoxIndices.Length / 3);
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Spiritus
{
    public class Camera : DrawableGameComponent
    {
        private static Camera activeCamera = null;

        // View and projection
        private Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 1.33f, 0.1f, 100f);
        private Matrix view = Matrix.Identity;

        // Character Position Variables
        private Vector3 position = new Vector3();
        private Vector3 characterPosition;
        private Vector3 angle = new Vector3(MathHelper.ToRadians(-22), 0, 0);

        // Camera Variables
        Vector3 forward = Vector3.Normalize(Vector3.Forward);
        Vector3 left = Vector3.Normalize(Vector3.Left);
        Vector3 up = Vector3.Normalize(Vector3.Up);

        private float turnSpeed = 60f;
        private float cameraSpeed = 2;
        private float staticCameraHeight;
        private float staticCameraHeightValue = 0.5f;
        private float currentCharacterScale = 1.0f;
        private float cameraDistanceFromCharacter = 0.7f;


        private bool movingCamera = false, newCameraMovement = true;
        private bool movingLookAtCamera = false, newCameraLookAtMovement = true;

        Vector3 lookAtPosition;

        public static bool turnLeft = false;
        public static bool turnRight = false;

        private Vector3 previousVector3Position;
        private Vector3 previousVector3CharacterPosition;

        private int battleSceneCameraSequence = 1;

        private bool initializedBattleCamera = false;

        // Random Number Generator
        private static Random RNG = new Random();

        float randomX;// = RNG.Next(1, 5);
        float randomY;//= RNG.Next(0, 3);
        float randomZ;// = RNG.Next(-6, -2);

        float randomLookAtX;// = RNG.Next(1, 5);
        float randomLookAtY;// = RNG.Next(0, 3);
        float randomLookAtZ;// = RNG.Next(-8, 0);

        public Vector3 getLookAtPosition
        {
            get { return lookAtPosition; }
        }

        public int CameraSequence
        {
            get { return battleSceneCameraSequence; }
        }

        public static Camera ActiveCamera
        {
            get { return activeCamera; }
            set { activeCamera = value; }
        }

        public float getCharacterScale
        {
            get { return currentCharacterScale; }
            set { currentCharacterScale = value; }
        }

        public Matrix Projection
        {
            get { return projection; }
        }

        public Matrix View
        {
            get { return view; }
        }

        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Vector3 Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        public Vector3 getForwardVector
        {
            get { return forward; }
        }
        public Vector3 getUpVector
        {
            get { return up; }
        }
        public Camera(Game game) : base(game)
        {
            if (ActiveCamera == null)
                ActiveCamera = this;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void MainCharacterCamera(Vector3 charPos)
        {
            characterPosition = charPos;

            staticCameraHeight = charPos.Y + staticCameraHeightValue;
        }

        private void MoveCameraLookAtPosition(GameTime gameTime, Vector3 newPosition, float speed)
        {
            // This allows the camera do only one movement at a time
            if (!movingLookAtCamera && newCameraLookAtMovement)
            {
                movingLookAtCamera = true;
            }

            lookAtPosition = Vector3.SmoothStep(lookAtPosition, newPosition, speed);
            if (Math.Abs(lookAtPosition.X - newPosition.X) < 0.1)
            {
                movingLookAtCamera = false;
                if (newCameraLookAtMovement)
                {
                    newCameraLookAtMovement = false;
                }
            }
        }

        private void MoveCameraPosition(GameTime gameTime, Vector3 newPosition, float speed)
        {
            // This allows the camera do only one movement at a time
            if (!movingCamera && newCameraMovement)
            {
                movingCamera = true;
            }

            position = Vector3.SmoothStep(position, newPosition, speed);

            if (Math.Abs(position.X - newPosition.X) < 0.1)
            {
                movingCamera = false;
                if (newCameraMovement)
                {
                    newCameraMovement = false;
                }
            }
        }

        public void ChangeBattleCameraPosition(int newPosition)
        {
            if (newCameraMovement == false)
            {
                battleSceneCameraSequence = newPosition;
                newCameraMovement = true;
            }

            if (newCameraLookAtMovement == false)
                newCameraLookAtMovement = true;
            
        }

        private void SelectRandomCameraStartingPosition()
        {
            randomX = RNG.Next(1, 5);
            randomY = RNG.Next(1, 3);
            randomZ = RNG.Next(-6, -2);
            
            randomLookAtX = RNG.Next(1, 5);
            randomLookAtY = RNG.Next(0, 3);
            randomLookAtZ = RNG.Next(-2, -1);

            Console.WriteLine("Random Camera Starting Positon: " + "(" + randomX + ", " + randomY + ", " + randomZ + ")");
            Console.WriteLine("Random Camera Starting Look At Position: " + "(" + randomLookAtX + ", " + randomLookAtY + ", " + randomLookAtZ + ")");
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.currentGameState != GameStates.Battle)
            {
                KeyboardState keyboard = Keyboard.GetState();

                forward = Vector3.Normalize(new Vector3((float)Math.Sin(-angle.Y), (float)Math.Sin(angle.X), (float)Math.Cos(-angle.Y)));
                Vector3 tempV3 = new Vector3();
                tempV3 = (forward * cameraDistanceFromCharacter) + characterPosition;

                if (GlobalVariables.DEBUG)
                    Console.WriteLine(characterPosition.Y);

                if (GameState.transitioning == false)
                {
                    if (keyboard.IsKeyDown(Keys.Left) && (
                        GameState.currentGameState == GameStates.World ||
                        GameState.currentGameState == GameStates.Dungeon))
                    {
                        angle.Y += MathHelper.ToRadians(-cameraSpeed * turnSpeed * (GameManager.delta * 0.001f));
                    }
                    if (keyboard.IsKeyDown(Keys.Right) && (
                        GameState.currentGameState == GameStates.World ||
                        GameState.currentGameState == GameStates.Dungeon))
                    {
                        angle.Y += MathHelper.ToRadians(cameraSpeed * turnSpeed * (GameManager.delta * 0.001f));
                    }
                }

                // This code is an attempt to smooth out the camera movement while the character is moving around the world. It works slightly better
                Vector3 newVector3Position = new Vector3(tempV3.X, staticCameraHeight, tempV3.Z);
                position = Vector3.SmoothStep(previousVector3Position, newVector3Position, 0.8f);
                position = Vector3.Lerp(previousVector3Position, newVector3Position, 0.5f);

                characterPosition = Vector3.SmoothStep(previousVector3CharacterPosition, characterPosition, 0.8f);
                characterPosition = Vector3.Lerp(previousVector3CharacterPosition, characterPosition, 0.5f);

                view = Matrix.Identity;
                view *= Matrix.CreateLookAt(position, characterPosition, up);
                view *= Matrix.CreateRotationX(angle.X);
                
                previousVector3Position = position;
                previousVector3CharacterPosition = characterPosition;

                initializedBattleCamera = false;

                base.Update(gameTime);
            }
            else if (GameState.currentGameState == GameStates.Battle)
            {
                if (GlobalVariables.DEBUG)
                {
                    Console.WriteLine("CAMERA POSITION: " + this.position);
                    Console.WriteLine("CAMERA LOOK AT: " + this.lookAtPosition);
                }

                if (!initializedBattleCamera)
                {
                    lookAtPosition = characterPosition;
                    SelectRandomCameraStartingPosition();
                    initializedBattleCamera = true;
                }
                
                float pX = -forward.Z;
                float pZ = forward.X;

                switch (battleSceneCameraSequence){
                    // Start Battle - Transition
                    case 1:
                        MoveCameraPosition(gameTime,
                            new Vector3(randomX, randomY, randomZ),
                            0.3f);
                        lookAtPosition = new Vector3(randomLookAtX, randomLookAtY, randomLookAtZ);
                        break;
                    // Move Camera to side view
                    case 2:
                        MoveCameraPosition(gameTime,
                            new Vector3(1.4f, 0.7f, -0.7f),
                            0.1f);
                        MoveCameraLookAtPosition(gameTime,
                            new Vector3(3.7f, -0.6f, -4),
                            0.1f);
                        break;
                    // Focus on Main Character
                    case 3:
                        MoveCameraPosition(gameTime, 
                            GlobalVariables.mainCharacterEncounterPosition,// + mainCharacterLocationOffset / 1.1f, 
                            20);
                        MoveCameraLookAtPosition(gameTime,
                            GlobalVariables.mainCharacterEncounterPosition, 
                            20);
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        MoveCameraPosition(gameTime,
                            new Vector3(2, 0, 2) + (new Vector3(-10 * forward.X, staticCameraHeightValue, 2 * forward.Z)),
                            25);
                        break;
                    case 7:
                        MoveCameraPosition(gameTime,
                            new Vector3(2, 0, 2) + (new Vector3(2 * forward.X, staticCameraHeightValue, -10 * forward.Z)),
                            25);
                        break;
                    case 8:
                        MoveCameraPosition(gameTime,
                            new Vector3(2, 0, 2) + (new Vector3(5 * forward.X, staticCameraHeightValue, 0 * forward.Z)),
                            25);
                        break;
                    case 9:
                        MoveCameraLookAtPosition(gameTime,
                            new Vector3(4.5f, -0.1f, -4),
                            25);
                        MoveCameraPosition(gameTime,
                            new Vector3(3.2f, 1.0f, -1.1f),
                            25);
                        
                        break;
                }

                view = Matrix.Identity;
                view *= Matrix.CreateLookAt(position, lookAtPosition, up);
                //view *= Matrix.CreateLookAt(position, GlobalVariables.mainCharacterEncounterPosition, up);

                base.Update(gameTime);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Spiritus.DataStructures;

namespace Spiritus
{
    public class BattleManager
    {
        private double battleStartTime;                 // Starting time based on gameTime
        private GameTime battleTime;                    // Battle Time - A copy of GameTime
        private List<BattleCharacter> listOfPlayerCharacters; // List of player characters - This will basically be static for most of the game so it will be managed by this class
        private List<BattleCharacter> listOfEnemyCharacters;  // List of enemy characters - This will have to be given

        private Game game;
        private InitiateBattleSpells initiateBattleSpells;

        private List<BattleCharacter> listOfCharacters;       // List of all characters (this is for sorting purposes)

        private List<string> PlayerCommandQueue;        // List of commands in queue for Player
        private List<string> EnemyCommandQueue;         // List of commands in queue for Enemy
        public List<FloatingNumber> listOfFloatingNumbers = new List<FloatingNumber>();
        private List<BattleSpell> listOfBattleSpells = new List<BattleSpell>();   // List of Battle Spells to update

        private Vector3[] EnemyPositions;
        private Vector3[] PlayerPositions;

        // Arrow Billboard Variables
        private Matrix arrowBillboard;
        private VertexPositionTexture[] arrowBoxVertices;
        private int[] arrowBoxIndices;

        private BattleCharacter currentArrowSelectedBattleCharacter;        // The battle character that's currently selected

        private bool targetingEnemy = false;

        public BattleGridPosition[] battleGridPositions = new BattleGridPosition[9];

        BasicEffect effect;

        private bool loadedNewBattle = false;

        public static bool readyForUI = false;

        // Battle Triggers
        private bool setSideCameraView = false;

        public bool getBattleUIReadyStatus
        {
            get { return readyForUI; }
        }

        public bool getBattleLoadedStatus
        {
            get { return loadedNewBattle; }
        }

        public bool getTargetEnemyStatus
        {
            get { return targetingEnemy; }
        }

        public List<BattleCharacter> getListOfPlayerBattleCharacters
        {
            get { return listOfPlayerCharacters; }
        }

        public List<BattleSpell> getListOfBattleSpells
        {
            get { return listOfBattleSpells; }
        }

        public List<FloatingNumber> getListOfFloatingNumbers
        {
            get { return listOfFloatingNumbers; }
        }
        // Constructor
        // Note: Technically, this can just be called Battle, 
        // since it's actually an instance of a battle, 
        // but it also manages the battle,
        // which is why I added "Manager" in the name
        public BattleManager(Character encounteredEnemy, GameTime gameTime, Game ga)
        {
            setPositions();             // Set positions (This is basically a grid formation, and preset to 6 locations for enemy positions

            effect = new BasicEffect(GameManager.graphicsDevice);
            game = ga;

            initiateBattleSpells = new InitiateBattleSpells(game.Content, this, game);

            CreateArrowBillboard();     // This creates the arrow
            CreateDefaultGridPositions();

            // Initialize lists
            listOfEnemyCharacters = CreateBattleCharacters(encounteredEnemy);
            listOfPlayerCharacters = new List<BattleCharacter>();

            // Populate listOfEnemyCharacters with given list of enemies

            for (int i = 0; i < listOfEnemyCharacters.Count; i++)
            {
                GameManager.characterManager.addBattleCharacter(listOfEnemyCharacters[i]);
            }

            listOfPlayerCharacters.Add(new BattleCharacter(GameManager.characterManager.mainCharacterWorld, new Vector2(PlayerPositions[0].X, PlayerPositions[0].Z), battleGridPositions[6], false));
            GameManager.characterManager.addBattleCharacter(listOfPlayerCharacters[0]);

            battleStartTime = gameTime.TotalGameTime.TotalSeconds;
            battleTime = gameTime;

            // Initialize the list of characters
            listOfCharacters = new List<BattleCharacter>();
            // Add the list of player and enemy characters to the master list
            listOfCharacters.AddRange(listOfPlayerCharacters);
            listOfCharacters.AddRange(listOfEnemyCharacters);

            ArrowSelectNewCharacter(listOfPlayerCharacters[0]);

            foreach (BattleGridPosition b in battleGridPositions)
                calculateNewTargets(b);

            loadedNewBattle = true;
        }

        public void ClearBattleManager()
        {
            EndBattle();
        }

        public void EndBattle()
        {
            
            // Clear all lists related to battle
            listOfBattleSpells.Clear();
            listOfCharacters.Clear();
            listOfEnemyCharacters.Clear();
            listOfPlayerCharacters.Clear();
            listOfFloatingNumbers.Clear();
            GlobalVariables.listOfBattleSprites.Clear();

            GameManager.firstBattleFlashDone = false;
            GameManager.secondBattleFlashDone = false;
            setSideCameraView = false;

            readyForUI = false;
            
            
        }

        private void KillEnemyCharacter(BattleCharacter battleCharacter)
        {
            // Later, this will include the code that will trigger the dying animation
            try
            {
                listOfCharacters.Remove(battleCharacter);
                listOfEnemyCharacters.Remove(battleCharacter);
                GameManager.characterManager.removeBattleCharacter(battleCharacter);
            }
            catch
            {
                Console.WriteLine("WARNING: CHARACTER DOES NOT EXIST");
            }
            
        }

        private void setPositions()
        {
            /*
             * 
             * 4
             * 3       P3   E3 E6
             * 2       P2   E2 E5
             * 1       P1   E1 E4  
             * 0    
             *      1   2   3   4   5   6   7   8   9   10
            */

            EnemyPositions = new Vector3[6];
            EnemyPositions[0] = new Vector3(3, 0, -2);
            EnemyPositions[1] = new Vector3(3, 0, -2.5f);
            EnemyPositions[2] = new Vector3(3, 0, -3);
            EnemyPositions[3] = new Vector3(3.5f, 0, -2f);
            EnemyPositions[4] = new Vector3(3.5f, 0, -2.5f);
            EnemyPositions[5] = new Vector3(3.5f, 0, -3f);

            PlayerPositions = new Vector3[3];
            PlayerPositions[0] = new Vector3(2, 0, -3);
            PlayerPositions[1] = new Vector3(2, 0, -2.5f);
            PlayerPositions[2] = new Vector3(2, 0, -2);
        }

        private double getBattleTime()
        {
            return battleTime.TotalGameTime.TotalSeconds - battleStartTime;
        }

        private void CreateArrowBillboard()
        {
            arrowBillboard = new Matrix();

            int charBoxHeight = 2;  // Vertices - min is 2
            int charBoxWidth = 2;   // Vertices - min is 2
            arrowBoxVertices = new VertexPositionTexture[charBoxHeight * charBoxWidth];

            arrowBoxVertices[0] = new VertexPositionTexture(
                        new Vector3(0.1f, 0.1f, 0),
                        new Vector2(0, 0));
            arrowBoxVertices[2] = new VertexPositionTexture(
                        new Vector3(0.1f, 0, 0),
                        new Vector2(0, 1));
            arrowBoxVertices[1] = new VertexPositionTexture(
                        new Vector3(0, 0.1f, 0),
                        new Vector2(1, 0));
            arrowBoxVertices[3] = new VertexPositionTexture(
                        new Vector3(0, 0, 0),
                        new Vector2(1, 1));

            arrowBoxIndices = new int[6];

            arrowBoxIndices[0] = 3;
            arrowBoxIndices[1] = 2;
            arrowBoxIndices[2] = 1;
            arrowBoxIndices[3] = 2;
            arrowBoxIndices[4] = 0;
            arrowBoxIndices[5] = 1;
        }

        private List<BattleCharacter> CreateBattleCharacters(Character character)
        {
            // enemyType will be the name of the type of enemy (ie. dog)
            // Later, I may want to add additional parameters such as difficulty, etc.

            List<BattleCharacter> battleList = new List<BattleCharacter>();
            Random rand = new Random();
            int randomNumber = rand.Next(1, 7);
            // Temporary - set enemy character to just 1
            //randomNumber = 1;
            for (int i = 0; i < randomNumber; i++)
            {
                BattleCharacter tempBattleCharacter = new BattleCharacter(
                    character,
                    new Vector2(EnemyPositions[i].X, EnemyPositions[i].Z), battleGridPositions[i], true);
                battleList.Add(tempBattleCharacter);

                tempBattleCharacter.characterType = CharacterType.Enemy;
            }

            return battleList;
        }

        // Called whenever a new battle character is being selected
        // This assumes that you can only select one character at a time.
        // This method selects the new character and automatically de-selects the previous character
        private void ArrowSelectNewCharacter(BattleCharacter newCharacter)
        {
            if (currentArrowSelectedBattleCharacter != null)
                currentArrowSelectedBattleCharacter.UnselectBattleCharacter();
            currentArrowSelectedBattleCharacter = newCharacter;
            currentArrowSelectedBattleCharacter.SelectBattleCharacter();
        }

        public void moveArrow(string direction)
        {
            int newCharacterPosition;

            switch (direction)
            {
                case "Left":
                    // Check if it's not null
                    if (currentArrowSelectedBattleCharacter.getGridPosition.getLeftPosition != null)
                    {
                        newCharacterPosition =
                            currentArrowSelectedBattleCharacter.getGridPosition.getLeftPosition.getGridPosition;

                        // Check if it is an enemy
                        if (currentArrowSelectedBattleCharacter.getGridPosition.getLeftPosition.isThisEnemy)
                            ArrowSelectNewCharacter(listOfEnemyCharacters[newCharacterPosition]);
                        else
                            ArrowSelectNewCharacter(listOfPlayerCharacters[newCharacterPosition - 6]);
                    }
                    break;
                case "Up":
                    if (currentArrowSelectedBattleCharacter.getGridPosition.getUpPosition != null)
                    {
                        newCharacterPosition =
                            currentArrowSelectedBattleCharacter.getGridPosition.getUpPosition.getGridPosition;

                        // Check if it is an enemy
                        if (currentArrowSelectedBattleCharacter.getGridPosition.getUpPosition.isThisEnemy)
                            ArrowSelectNewCharacter(listOfEnemyCharacters[newCharacterPosition]);
                        else
                            ArrowSelectNewCharacter(listOfPlayerCharacters[newCharacterPosition - 6]);
                    }
                    break;
                case "Down":
                    if (currentArrowSelectedBattleCharacter.getGridPosition.getDownPosition != null)
                    {
                        newCharacterPosition =
                            currentArrowSelectedBattleCharacter.getGridPosition.getDownPosition.getGridPosition;

                        // Check if it is an enemy
                        if (currentArrowSelectedBattleCharacter.getGridPosition.getDownPosition.isThisEnemy)
                            ArrowSelectNewCharacter(listOfEnemyCharacters[newCharacterPosition]);
                        else
                            ArrowSelectNewCharacter(listOfPlayerCharacters[newCharacterPosition - 6]);
                    }
                    break;
                case "Right":
                    if (currentArrowSelectedBattleCharacter.getGridPosition.getRightPosition != null)
                    {
                        newCharacterPosition =
                            currentArrowSelectedBattleCharacter.getGridPosition.getRightPosition.getGridPosition;

                        // Check if it is an enemy
                        if (currentArrowSelectedBattleCharacter.getGridPosition.getRightPosition.isThisEnemy)
                            ArrowSelectNewCharacter(listOfEnemyCharacters[newCharacterPosition]);
                        else
                            ArrowSelectNewCharacter(listOfPlayerCharacters[newCharacterPosition - 6]);
                    }
                    break;
            }

        }

        private void UpdateArrowBillboard()
        {
            if (currentArrowSelectedBattleCharacter != null)
            {
                Vector3 leftVector = Vector3.Cross(Vector3.Up, Camera.ActiveCamera.getForwardVector);
                // Check if character is an enemy or a player
                // Since the arrow will be shifted slightly if it's a player

                Vector3 arrowOffsetVector;
                if (currentArrowSelectedBattleCharacter.characterType == CharacterType.Enemy)
                    arrowOffsetVector = new Vector3(0, 0.3f, 0);
                else
                    arrowOffsetVector = new Vector3(0.05f, 0.3f, 0);
                arrowBillboard = Matrix.CreateBillboard(
                    currentArrowSelectedBattleCharacter.Location + arrowOffsetVector,
                    Camera.ActiveCamera.Position,
                    Camera.ActiveCamera.getUpVector,
                    Camera.ActiveCamera.getForwardVector);
                
            }
        }

        private void DrawSelectionArrow(GraphicsDevice g)
        {
            // This is for the Battle Arrow Enemy Selector
            UpdateArrowBillboard();

            // To draw the arrow
            effect.World = arrowBillboard;
            effect.View = Camera.ActiveCamera.View;
            effect.Projection = Camera.ActiveCamera.Projection;
            effect.Texture = GameManager.textureManager.getTexture("GUI/RedTriangle", 2);
            effect.TextureEnabled = true;

            effect.CurrentTechnique.Passes[0].Apply();

            g.DrawUserIndexedPrimitives<VertexPositionTexture>(
                PrimitiveType.TriangleList,
                arrowBoxVertices,
                0,
                arrowBoxVertices.Length,
                arrowBoxIndices,
                0,
                arrowBoxIndices.Length / 3);
        }

        // This will contain all the update code for the battle
        public void Update()
        {
            // Most of these are temporary / need to be refactored

            // Sets the 
            if (getBattleTime() < 1)
            {
                listOfPlayerCharacters[0].animationState = 1;
                Camera.ActiveCamera.ChangeBattleCameraPosition(1);
            }
            if (getBattleTime() > 1 && !setSideCameraView)
            {
                Camera.ActiveCamera.ChangeBattleCameraPosition(2);
                setSideCameraView = true;
            }

            if (readyForUI)
            {
                DrawSelectionArrow(GameManager.graphicsDevice);               // Draw selection arrow
            }
            // Puts UI on the screen
            if (getBattleTime() > 2 && setSideCameraView && !readyForUI)
                readyForUI = true;
            
            // Switches animation state for characters based on the Camera angle
            if (Camera.ActiveCamera.CameraSequence == 2)
                listOfPlayerCharacters[0].animationState = 3;
            if (Camera.ActiveCamera.CameraSequence == 1)
                listOfPlayerCharacters[0].animationState = 1;

            // Update Character Action Bars
            // This will be a function that runs through each character and
            // Updates their action bar accordingly.
            UpdateActionBars();

            // Update Enemy Actions
            // This will be a function that runs through each enemy character, and if 
            // they are ready for action, it will execute an action.
            UpdateEnemyActions();

            // Draw Floating Numbers
            for (int i = listOfFloatingNumbers.Count - 1; i >= 0; i--)
            {
                // Check if the floating number is done drawing, then remove it.
                if (!listOfFloatingNumbers[i].DrawFloatingNumber(battleTime))
                {
                    listOfFloatingNumbers.Remove(listOfFloatingNumbers[i]);
                }
            }

            // Call Update Function for each Battle Spell
            foreach (BattleSpell b in listOfBattleSpells)
            {
                b.Update(battleTime);
            }

            // Clean up Inactive Battle Spells
            for (int i = listOfBattleSpells.Count - 1; i >= 0; i--)
            {
                if (listOfBattleSpells[i].isDoneMovement)
                    listOfBattleSpells.Remove(listOfBattleSpells[i]);
            }
        }

        private void UpdateEnemyActions()
        {
            foreach (BattleCharacter c in listOfEnemyCharacters)
            {
                // Check if enemy character is ready
                if (c.getReadyStatus)
                {
                    // Run the take action command
                    c.takeActionAI(listOfPlayerCharacters, listOfEnemyCharacters, listOfFloatingNumbers, initiateBattleSpells);
                }
            }
        }

        private void UpdateActionBars()
        {
            // Update Enemy Action Bars
            foreach (BattleCharacter c in listOfEnemyCharacters)
            {
                if (!c.getReadyStatus)          // Check if character is not already ready for action
                    c.incrementActionBar();     // Increment the character's action bar
            }

            // Update Player Action Bars
            foreach (BattleCharacter c in listOfPlayerCharacters)
            {
                if (!c.getReadyStatus)          // Check if character is not already ready for action
                    c.incrementActionBar();     // Increment the character's action bar
            }
        }
        
        
        #region Calculate Grid Targets

        private void dynamicGridLink(BattleGridPosition b)
        {
            // Preset grid links

            // Store the array
            byte[,] array = b.getPositionPriorityArray;

            // Iterate through the directions
            for (int i = 0; i < 4 ; i++)
            {
                // Iterate through the elements
                for (int j = 1; j < array.Length / 4; j++)
                {
                    // Obtain the index
                    int index = array[i, j];

                    switch (i)
                    {
                        case 0:  // Up
                            // Check if the index is 9 (null)
                            if (index == 9)
                            {
                                b.setUpLocation(null);    // Set the position to null
                                j = array.Length;           // Set j so the loop exits
                            }
                            else
                            {
                                // Check if the character is in Players or Enemies array
                                if (index < 6)
                                {
                                    // Check if the character is within the range of the array
                                    if (index < listOfEnemyCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfEnemyCharacters[index].isCharacterDead)
                                        {
                                            b.setUpLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                                else // Character is a player
                                {
                                    // Check if the character is within the range of the array
                                    if ((index - 6) < listOfPlayerCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfPlayerCharacters[(index - 6)].isCharacterDead)
                                        {
                                            b.setUpLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                            }
                            break;
                        case 1:  // Down
                            // Check if the index is 9 (null)
                            if (index == 9)
                            {
                                b.setDownLocation(null);    // Set the position to null
                                j = array.Length;           // Set j so the loop exits
                            }
                            else
                            {
                                // Check if the character is in Players or Enemies array
                                if (index < 6)
                                {
                                    // Check if the character is within the range of the array
                                    if (index < listOfEnemyCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfEnemyCharacters[index].isCharacterDead)
                                        {
                                            b.setDownLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                                else // Character is a player
                                {
                                    // Check if the character is within the range of the array
                                    if ((index - 6) < listOfPlayerCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfPlayerCharacters[(index - 6)].isCharacterDead)
                                        {
                                            b.setDownLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                            }
                            break;
                        case 2:  // Left
                            // Check if the index is 9 (null)
                            if (index == 9)
                            {
                                b.setLeftLocation(null);    // Set the position to null
                                j = array.Length;           // Set j so the loop exits
                            }
                            else
                            {
                                // Check if the character is in Players or Enemies array
                                if (index < 6)
                                {
                                    // Check if the character is within the range of the array
                                    if (index < listOfEnemyCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfEnemyCharacters[index].isCharacterDead)
                                        {
                                            b.setLeftLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                                else // Character is a player
                                {
                                    // Check if the character is within the range of the array
                                    if ((index - 6) < listOfPlayerCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfPlayerCharacters[(index - 6)].isCharacterDead)
                                        {
                                            b.setLeftLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                            }
                            break;
                        case 3:  // Right
                            // Check if the index is 9 (null)
                            if (index == 9)
                            {
                                b.setRightLocation(null);    // Set the position to null
                                j = array.Length;           // Set j so the loop exits
                            }
                            else
                            {
                                // Check if the character is in Players or Enemies array
                                if (index < 6)
                                {
                                    // Check if the character is within the range of the array
                                    if (index < listOfEnemyCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfEnemyCharacters[index].isCharacterDead)
                                        {
                                            b.setRightLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                                else // Character is a player
                                {
                                    // Check if the character is within the range of the array
                                    if ((index - 6) < listOfPlayerCharacters.Count)
                                    {
                                        // Check if that character is still alive
                                        if (!listOfPlayerCharacters[(index - 6)].isCharacterDead)
                                        {
                                            b.setRightLocation(battleGridPositions[index]);  // Set the position
                                            j = array.Length;   // Set j so the loop exits
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }

        }

        public void calculateNewTargets(BattleGridPosition b)
        {
            dynamicGridLink(b);
        }

        ////////////////////////////////
        //  Visual of Grid Locations  //
        ////////////////////////////////
        //   Players       Enemies    //
        //                            //
        //      6           2   5     //      
        //                            //
        //      7           1   4     //
        //                            //    
        //      8           0   3     //
        ////////////////////////////////

        private void CreateSelectionLinkPrioritiesArrays()
        {
            // These will basically be hardcoded based on the grid provided.

            byte[][,] gridPriorityArrays = new byte[9][,] 
            {
                // For the two dimensional array:
                // (Direction, priority)
                // Directions
                // 0 = Up
                // 1 = Down
                // 2 = Left
                // 3 = Right

                // List of BattleGridPositions from 0 - 8
                
                // Note: 9 means null since byte is not nullable

                new byte[,] { {0, 1, 2, 5, 9, 9}, {1, 8, 9, 9, 9, 9}, {2, 8, 7, 1, 2, 6}, {3, 3, 4, 5, 9, 9} }, // 0
                new byte[,] { {0, 2, 5, 6}, {1, 0, 8, 9}, {2, 7, 6, 8}, {3, 4, 0, 3} }, // 1
                new byte[,] { {0, 5, 9, 9}, {1, 1, 0, 3}, {2, 6, 7, 8}, {3, 5, 4, 1} }, // 2
                new byte[,] { {0, 4, 5, 9, 9, 9}, {1, 0, 9, 9, 9, 9}, {2, 0, 1, 8, 7, 6}, {3, 9, 9, 9, 9, 9} }, // 3
                new byte[,] { {0, 5, 2, 9, 9, 9, 9}, {1, 3, 0, 9, 9, 9, 9}, {2, 1, 5, 2, 8, 7, 6}, {3, 3, 9, 9, 9, 9, 9} }, // 4
                new byte[,] { {0, 9, 9, 9, 9, 9}, {1, 4, 1, 3, 0, 9}, {2, 2, 1, 6, 7, 8}, {3, 4, 3, 9, 9, 9} }, // 5
                new byte[,] { {0, 9, 9, 9, 9, 9, 9}, {1, 7, 8, 9, 9, 9, 9}, {2, 9, 9, 9, 9, 9, 9}, {3, 2, 1, 0, 5, 4, 3} }, // 6
                new byte[,] { {0, 6, 9, 9, 9, 9, 9}, {1, 8, 0, 9, 9, 9, 9}, {2, 6, 9, 9, 9, 9, 9}, {3, 1, 0, 2, 3, 4, 5} }, // 7
                new byte[,] { {0, 7, 9, 9, 9, 9, 9}, {1, 9, 9, 9, 9, 9, 9}, {2, 7, 8, 9, 9, 9, 9}, {3, 0, 1, 2, 3, 4, 5} }, // 8
            };

            // Iterate through BattleGridPositions
            for (int i = 0; i < battleGridPositions.Length; i++)
            {
                // Set the 2 dimensional array for each BattleGridPosition
                battleGridPositions[i].setLinkPrioritiesArray(gridPriorityArrays[i]);
            }
        }

        private void CreateDefaultGridPositions()
        {
            // First create the battle positions array
            for (byte i = 0; i < 9; i++)
            {
                // Check if the position is a player position
                if (i >= 6)
                    battleGridPositions[i] = new BattleGridPosition(i, false);
                else
                    battleGridPositions[i] = new BattleGridPosition(i, true);
            }

            // Create the Priorities Array for linking
            CreateSelectionLinkPrioritiesArrays();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace Spiritus
{
    public class TextureManager
    {
        private Dictionary<string, Texture2D> worldTextures;
        private Dictionary<string, Texture2D> characterTextures;
        private Dictionary<string, Texture2D> UITextures;
        private Dictionary<string, Texture2D> SpellTextures;

        private Dictionary<string, Texture2D>[] listOfTextureDictionaries;

        public TextureManager()
        {
            listOfTextureDictionaries = new Dictionary<string, Texture2D>[4];

            worldTextures = new Dictionary<string, Texture2D>();
            characterTextures = new Dictionary<string, Texture2D>();
            UITextures = new Dictionary<string, Texture2D>();
            SpellTextures = new Dictionary<string,Texture2D>();

            listOfTextureDictionaries[0] = worldTextures;
            listOfTextureDictionaries[1] = characterTextures;
            listOfTextureDictionaries[2] = UITextures;
            listOfTextureDictionaries[3] = SpellTextures;
        }
        
        public void LoadKeyTextures(Game game)
        {
            // World Textures
            LoadTextureIntoMemory(game, 0, "newheightmap");
            LoadTextureIntoMemory(game, 0, "Heightmaps/BattleSceneHeightmap");
            LoadTextureIntoMemory(game, 0, "Water");
            LoadTextureIntoMemory(game, 0, "DungeonTile");
            LoadTextureIntoMemory(game, 0, "Grass");
            LoadTextureIntoMemory(game, 0, "Mountain");
            
            // Character Textures
            LoadTextureIntoMemory(game, 1, "laila");
            LoadTextureIntoMemory(game, 1, "Haruhi");
            LoadTextureIntoMemory(game, 1, "Character");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Stand1");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Stand2");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Walk1");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Walk2");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Walk3");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/Walk4");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/BattleSide1");
            LoadTextureIntoMemory(game, 1, "CharacterSprites/MainCharacter/BattleSide2");

            // UI Textures
            LoadTextureIntoMemory(game, 2, "FlashScreen");
            LoadTextureIntoMemory(game, 2, "GUI/DialogBox");
            LoadTextureIntoMemory(game, 2, "HaruhiPortrait");
            LoadTextureIntoMemory(game, 2, "GUI/BlueBox200x500");
            LoadTextureIntoMemory(game, 2, "GUI/BlueButton100x50");
            LoadTextureIntoMemory(game, 2, "GUI/YellowButton100x50");
            LoadTextureIntoMemory(game, 2, "GUI/BlueBox800x600");
            LoadTextureIntoMemory(game, 2, "GUI/800x300BattleMenu");
            LoadTextureIntoMemory(game, 2, "GUI/BottomBattleMenu");
            LoadTextureIntoMemory(game, 2, "GUI/RedTriangle");
            LoadTextureIntoMemory(game, 2, "GUI/DarkGreyHealthBar");
            LoadTextureIntoMemory(game, 2, "GUI/GreenHealthBar");
            LoadTextureIntoMemory(game, 2, "GUI/RedActionBar");

            // Spell and Ability Textures
            LoadTextureIntoMemory(game, 3, "Assets/Spells/IceLance");

            // Some Global Variables
            GlobalVariables.buttonSelected = getTexture("GUI/YellowButton100x50", 2);
            GlobalVariables.buttonNotSelected = getTexture("GUI/BlueButton100x50", 2);
        }
        public void LoadTextureIntoMemory(Game game, int type, string name)
        {
            // int type refers to the position of the desired 
            // dictionary in the Array listOfTextureDictionaries

            // Current, they are as follows:
            // 0 - worldTextures        -   World Textures are textures that may be randomly scattered around
            // 1 - characterTextures    -   Character Textures are textures specifically for Characters
            // 2 - UI Textures          -   Textures that are associated specifically to UI'
            // 3 - Spell Textures       -   Textures that are used for spell and ability effects
            Texture2D tempTexture;
            tempTexture = game.Content.Load<Texture2D>(name);

            listOfTextureDictionaries[type].Add(name, tempTexture);            
        }
 
        public void UnloadTextureOutOfMemory(Game game, string name, int type)
        {
            getTexture(name, type).Dispose();
        }

        public Texture2D getTexture(string name, int type)
        {
            Texture2D returnTexture;
            listOfTextureDictionaries[type].TryGetValue(name, out returnTexture);

            if (returnTexture == null)
                Console.Error.WriteLine("WARNING: COULD NOT FIND TEXTURE NAME:" + name + "    TEXTURE: " + returnTexture);
            return returnTexture;
        }



        /*
         * 
         * The following is code for unique textures
         * Basically I had attempted to do some efficient management with textures
         * Disposing of them as required, but its not working out very well
         * 
         * Since this game will have fairly limited textures, I felt that it wasn't worth it to try and do garbage collection
         * 
         * 
         * 
 
         * /*
        public void LoadBattleUniqueTextures(Game game)
        {
            LoadingTextures = true;
            LoadUniqueTextureIntoMemory(game, "Heightmaps/BattleSceneHeightmap", "heightmap");
            LoadUniqueTextureIntoMemory(game, "DungeonTile", "water");
            LoadUniqueTextureIntoMemory(game, "DungeonTile", "dungeon");
            LoadUniqueTextureIntoMemory(game, "DungeonTile", "grass");
            LoadUniqueTextureIntoMemory(game, "DungeonTile", "mountain");
            LoadingTextures = false;
        }

        public void LoadWorldUniqueTextures(Game game)
        {
            LoadingTextures = true;
            LoadUniqueTextureIntoMemory(game, "newheightmap", "heightmap");
            LoadUniqueTextureIntoMemory(game, "Water", "water");
            LoadUniqueTextureIntoMemory(game, "DungeonTile", "dungeon");
            LoadUniqueTextureIntoMemory(game, "Grass", "grass");
            LoadUniqueTextureIntoMemory(game, "Mountain", "mountain");
            LoadingTextures = false;
        }

         public void LoadUniqueTextureIntoMemory(Game game, string name, string uniqueName)
 {
     // If the texture already exists, unload it first
     if (uniqueTextures.ContainsKey(uniqueName))
     {
         UnloadUniqueTextureOutOfMemory(game, uniqueName);

         uniqueTextures.Remove(uniqueName);
         uniqueTexturesMap.Remove(uniqueName);
     }

     Texture2D tempTexture;
     tempTexture = game.Content.Load<Texture2D>(name);

     uniqueTextures.Add(uniqueName, tempTexture);
     uniqueTexturesMap.Add(uniqueName, name);

     if (uniqueName == "grass")
         currentGrassTexture = getUniqueTexture("grass");
     if (uniqueName == "water")
         currentWaterTexture = getUniqueTexture("water");
     if (uniqueName == "heightmap")
         currentHeightMap = getUniqueTexture("heightmap");
     if (uniqueName == "mountain")
         currentMountainTexture = getUniqueTexture("mountain");
     if (uniqueName == "dirt")
         currentDirtTexture = getUniqueTexture("dirt");
     if (uniqueName == "dungeon")
         currentDungeonTexture = getUniqueTexture("dungeon");
 }

 public void UnloadUniqueTextureOutOfMemory(Game game, string name)
 {
            
     string oldName;
     uniqueTexturesMap.TryGetValue(name, out oldName);
     getUniqueTexture(oldName).Dispose();
             
     getUniqueTexture(name).Dispose();
 }
 

        
public Texture2D getUniqueTexture(string uniqueName)
{
    Texture2D returnTexture;
    uniqueTextures.TryGetValue(uniqueName, out returnTexture);
    return returnTexture;
}
*/
    }
}

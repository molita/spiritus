﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
namespace Spiritus
{
    public class CharacterManager
    {
        // Predefined Characters
        // These characters are unique and usually always exist
        public Character mainCharacterWorld; 
        public static BattleCharacter mainCharacterBattle;
        public Character mainGirlCharacter, mainGuyCharacter;

        // Animation Textures for predefined characters
        // These will be hardcoded (no current plans to make it dynamic)
        private Texture2D[] mainCharacterWorldAnimationSet = new Texture2D[8];
        private Vector3[] mainCharacterWorldAnimationSetVector3 = new Vector3[3];

        // Other Predefined Character variables
        private Vector2 mainCharacterStartLocation = new Vector2(35, -35);

        // List of Characters to Draw
        private List<Character> listOfCharacters = new List<Character>();
        private List<BattleCharacter> listOfBattleCharacters = new List<BattleCharacter>();

        public List<Character> getListOfCharacters
        {
            get { return listOfCharacters; }
        }

        public List<BattleCharacter> getListOfBattleCharacters
        {
            get { return listOfBattleCharacters; }
        }

        // Temporary Character Variable for Checking Main Character
        private Character checkCharacter;

        public void InitializeCoreCharacters(ContentManager contentManager)
        {
            // Test Characters
            Texture2D sakura = contentManager.Load<Texture2D>("Character");
            Texture2D haruhi = contentManager.Load<Texture2D>("Haruhi");

            Texture2D[] testCharacterAnimationSet = new Texture2D[1];
            testCharacterAnimationSet[0] = sakura;
            Vector3[] testCharacterAnimationSetVector3 = new Vector3[1];
            testCharacterAnimationSetVector3[0] = new Vector3(0, 1, 1);

            Texture2D[] haruhiAnimationSet = new Texture2D[1];
            haruhiAnimationSet[0] = haruhi;
            Vector3[] haruhiAnimationSetVector3 = new Vector3[1];
            haruhiAnimationSetVector3[0] = new Vector3(0, 1, 1);

            Character testCharacter, testCharacter2, testCharacter3, testCharacter4, testCharacter5, testCharacter6, testCharacter7;

            testCharacter = new Character(
                "sakura",
                GameManager.textureManager.getTexture("Character", 1),
                new Vector2(36, -36),
                testCharacterAnimationSet,
                testCharacterAnimationSetVector3,
                0.3f, 1);
            testCharacter.characterType = CharacterType.Enemy;

            testCharacter2 = new Character(
                "sakura",
                GameManager.textureManager.getTexture("Character", 1),
                new Vector2(45, -45),
                testCharacterAnimationSet,
                testCharacterAnimationSetVector3,
                0.3f, 1);
            testCharacter2.characterType = CharacterType.Enemy;

            testCharacter3 = new Character(
                "Haruhi 1",
                GameManager.textureManager.getTexture("Haruhi", 1),
                new Vector2(48, -48),
                haruhiAnimationSet,
                haruhiAnimationSetVector3,
                0.15f, 2);
            testCharacter3.characterType = CharacterType.NPC;

            testCharacter4 = new Character(
                "Haruhi 2",
                GameManager.textureManager.getTexture("Haruhi", 1),
                new Vector2(52f, -52f),
                haruhiAnimationSet,
                haruhiAnimationSetVector3,
                0.15f, 2);
            testCharacter4.characterType = CharacterType.NPC;

            testCharacter5 = new Character(
                "Haruhi 3",
                GameManager.textureManager.getTexture("Haruhi", 1),
                new Vector2(38, -38),
                haruhiAnimationSet,
                haruhiAnimationSetVector3,
                0.15f, 2);
            testCharacter5.characterType = CharacterType.NPC;

            testCharacter4.CharacterPortrait = "HaruhiPortrait";
            testCharacter3.CharacterPortrait = "HaruhiPortrait";
            testCharacter5.CharacterPortrait = "HaruhiPortrait";

            testCharacter6 = new Character(
                "sakura",
                GameManager.textureManager.getTexture("Character", 1),
                new Vector2(45, -46),
                testCharacterAnimationSet,
                testCharacterAnimationSetVector3,
                0.3f, 1);
            testCharacter6.characterType = CharacterType.Enemy;

            testCharacter7 = new Character(
                "sakura",
                GameManager.textureManager.getTexture("Character", 1),
                new Vector2(45, -47),
                testCharacterAnimationSet,
                testCharacterAnimationSetVector3,
                0.3f, 1);
            testCharacter7.characterType = CharacterType.Enemy;

            addCharacterToList(testCharacter);
            addCharacterToList(testCharacter2);
            addCharacterToList(testCharacter3);
            addCharacterToList(testCharacter4);
            addCharacterToList(testCharacter5);
            addCharacterToList(testCharacter6);
            addCharacterToList(testCharacter7);
        }

        public void InitializeCoreMainCharacter(ContentManager contentManager)
        {
            
            // Set up the Animation Set
            mainCharacterWorldAnimationSet[0] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Stand1", 1);
            mainCharacterWorldAnimationSet[1] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Stand2", 1);
            mainCharacterWorldAnimationSet[2] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Walk1", 1);
            mainCharacterWorldAnimationSet[3] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Walk2", 1);
            mainCharacterWorldAnimationSet[4] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Walk3", 1);
            mainCharacterWorldAnimationSet[5] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/Walk4", 1);
            // Battle Animation
            mainCharacterWorldAnimationSet[6] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/BattleSide1", 1);
            mainCharacterWorldAnimationSet[7] = GameManager.textureManager.getTexture("CharacterSprites/MainCharacter/BattleSide2", 1);
            
            mainCharacterWorldAnimationSetVector3[0] = new Vector3(0, 2, 1);
            mainCharacterWorldAnimationSetVector3[1] = new Vector3(2, 4, 2);
            mainCharacterWorldAnimationSetVector3[2] = new Vector3(6, 2, 3);
           
            // Create the Character
            mainCharacterWorld = new Character(
                "mainCharacter", 
                GameManager.textureManager.getTexture("laila", 1),
                mainCharacterStartLocation,
                mainCharacterWorldAnimationSet,
                mainCharacterWorldAnimationSetVector3,
                .15f, 2);

            addCharacterToList(mainCharacterWorld);
        }

        private void addCharacterToList(Character character)
        {
            listOfCharacters.Add(character);
        }

        private void removeCharacterFromList(Character character)
        {
            listOfCharacters.Remove(character);
            GlobalVariables.listOfWorldSprites.Remove(character);
        }

        public void addBattleCharacter(BattleCharacter battleCharacter)
        {
            listOfBattleCharacters.Add(battleCharacter);
            GlobalVariables.listOfBattleSprites.Add(battleCharacter);
        }

        public void removeBattleCharacter(BattleCharacter battleCharacter)
        {
            listOfBattleCharacters.Remove(battleCharacter);
            GlobalVariables.listOfBattleSprites.Remove(battleCharacter);
        }

        public void clearBattleCharacters()
        {
            listOfBattleCharacters.Clear();
        }

        public void removeWorldCharacter(Character character)
        {
            if (listOfCharacters.Contains(character))
                Console.WriteLine("The list contains this character!");
            removeCharacterFromList(character);

            if (listOfCharacters.Contains(character))
                Console.WriteLine("The list STILL contains this character after removing it!");
        }

        public void CheckInteractingCharacter()
        {
            listOfCharacters.Sort();
            if (listOfCharacters[1] == mainCharacterWorld)
                checkCharacter = listOfCharacters[0];
            else
                checkCharacter = listOfCharacters[1];

            if (checkCharacter.getDistanceFromCamera < 1.0f && checkCharacter.characterType == CharacterType.NPC)
            {
                GlobalVariables.interactedCharacter = checkCharacter;
            }
            else
            {
                GlobalVariables.interactedCharacter = null;
            }

            if (GlobalVariables.interacting && GlobalVariables.interactedCharacter != null)
            {
                GameManager.userInterface.ShowWindow("bottomDialogBox");
                GameManager.userInterface.ShowWindow("characterPortrait");
            }
            else
            {
                GlobalVariables.interacting = false;
                GlobalVariables.CharacterDialog.dialog.resetCurrentDialog();
                GameManager.userInterface.HideWindow("bottomDialogBox");
                GameManager.userInterface.HideWindow("characterPortrait");
            }
        }

    }
}

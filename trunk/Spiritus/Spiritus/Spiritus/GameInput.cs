﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Spiritus;

using Spiritus.GameObjects;

namespace Spiritus
{
    // This class will handle all the Keyboard Input

    class GameInput
    {
        private static int numberOfFailedMoveAttempts;
        private static Plane intersectingPlane;
        public static Character intersectingCharacter;
        private static Vector3 tempVector3;

        private static KeyboardState keyboardInput;
        private static KeyboardState previousKeyboardInput;

        public static void gameInput(GameTime gameTime, BattleManager battleManager)
        {
            keyboardInput = Keyboard.GetState();

            #region Movement Keys - Up Down Left Right
            if (keyboardInput.IsKeyDown(Keys.Up) ||
                keyboardInput.IsKeyDown(Keys.Down) ||
                keyboardInput.IsKeyDown(Keys.Left) ||
                keyboardInput.IsKeyDown(Keys.Right))
            {
                // Sets the Pre-emptive vector check for movement if Game State is World or Dungeon
                if (GameState.currentGameState == GameStates.World ||
                    GameState.currentGameState == GameStates.Dungeon)
                {
                    Vector3 forward = GameManager.characterManager.mainCharacterWorld.Direction;
                    tempVector3 = (Camera.ActiveCamera.getForwardVector) * GlobalVariables.globalKeyboardMovementUnit;
                }

                if (GameState.transitioning == false)
                {
                    switch (GameState.currentGameState)
                    {
                        case GameStates.World: // World
                            if (keyboardInput.IsKeyDown(Keys.Up))
                                MoveCharacter(1, gameTime, tempVector3);    // Move Forward
                            if (keyboardInput.IsKeyDown(Keys.Down))
                                MoveCharacter(2, gameTime, tempVector3);    // Move Backwards
                            break;
                        case GameStates.Dungeon: // Dungeon
                            if (keyboardInput.IsKeyDown(Keys.Up))
                                MoveCharacter(1, gameTime, tempVector3);    // Move Forward
                            if (keyboardInput.IsKeyDown(Keys.Down))
                                MoveCharacter(2, gameTime, tempVector3);    // Move Backwards
                            break;
                        case GameStates.MainMenu: // Main Menu
                        case GameStates.Battle: // Battle
                        case GameStates.WorldPauseMenu: // World Pause Menu
                                if (PressKey(Keys.Up))
                                {
                                    if (GameManager.userInterface.CurrentSelectedButton.upButton != null)
                                    {
                                        GameManager.userInterface.CurrentSelectedButton.Selected = false;
                                        GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.CurrentSelectedButton.upButton;
                                        GameManager.userInterface.CurrentSelectedButton.Selected = true;
                                    }
                                }
                                else if (PressKey(Keys.Down))
                                {
                                    if (GameManager.userInterface.CurrentSelectedButton.downButton != null)
                                    {
                                        GameManager.userInterface.CurrentSelectedButton.Selected = false;
                                        GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.CurrentSelectedButton.downButton;
                                        GameManager.userInterface.CurrentSelectedButton.Selected = true;
                                    }
                                }
                                else if (PressKey(Keys.Left))
                                {
                                    if (GameManager.userInterface.CurrentSelectedButton.leftButton != null)
                                    {
                                        GameManager.userInterface.CurrentSelectedButton.Selected = false;
                                        GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.CurrentSelectedButton.leftButton;
                                        GameManager.userInterface.CurrentSelectedButton.Selected = true;
                                    }
                                }
                                else if (PressKey(Keys.Right))
                                {
                                    if (GameManager.userInterface.CurrentSelectedButton.rightButton != null)
                                    {
                                        GameManager.userInterface.CurrentSelectedButton.Selected = false;
                                        GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.CurrentSelectedButton.rightButton;
                                        GameManager.userInterface.CurrentSelectedButton.Selected = true;
                                    }
                                }
                            break;
                        case GameStates.Cutscene: // Cutscene?
                            break;
                    }
                }
            }
            else
                GameManager.characterManager.mainCharacterWorld.animationState = 1;    // Standing Animation
            #endregion

            if (PressKey(Keys.W) ||
                PressKey(Keys.A) ||
                PressKey(Keys.S) ||
                PressKey(Keys.D))
            {
                switch (GameState.currentGameState)
                {
                    case GameStates.World: // World
                        break;
                    case GameStates.Dungeon: // Dungeon
                        break;
                    case GameStates.MainMenu: // Main Menu
                    case GameStates.Battle: // Battle

                        if (PressKey(Keys.A))
                            battleManager.moveArrow("Left");
                        if (PressKey(Keys.W))
                            battleManager.moveArrow("Up");
                        if (PressKey(Keys.S))
                            battleManager.moveArrow("Down");
                        if (PressKey(Keys.D))
                            battleManager.moveArrow("Right");
                            break;
                    case GameStates.WorldPauseMenu: // World Pause Menu
                        break;
                    case GameStates.Cutscene: // Cutscene?
                        break;
                }
            }

            if (PressKey(Keys.Enter))                    // Interact with Environment
            {
                switch (GameState.currentGameState)
                {
                    case GameStates.MainMenu:
                        break;
                    case GameStates.World:
                    case GameStates.Dungeon:
                        if (GlobalVariables.interactedCharacter != null)    // Interact with Character
                            GlobalVariables.interacting = true;

                        if (GlobalVariables.interacting == true)
                        {
                            GameManager.userInterface.SetDialogText(GlobalVariables.CharacterDialog.dialog.LoadDialog(GlobalVariables.interactedCharacter.NameSingleWord));
                        }

                        break;
                    case GameStates.Battle:
                        break;
                    case GameStates.Cutscene:
                        break;
                    case GameStates.WorldPauseMenu:
                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "worldPauseMenuExitButton")       // World Pause Exit Button
                            GameState.setNextGameState(GameState.previousGameState);

                        // Different Windows
                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "button1")
                        {
                            GameManager.userInterface.ShowWindow("button1Menu");
                            GameManager.userInterface.HideWindow("button2Menu");
                            GameManager.userInterface.HideWindow("button3Menu");
                            GameManager.userInterface.HideWindow("button4Menu");
                        }
                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "button2")
                        {
                            GameManager.userInterface.ShowWindow("button2Menu");
                            GameManager.userInterface.HideWindow("button1Menu");
                            GameManager.userInterface.HideWindow("button3Menu");
                            GameManager.userInterface.HideWindow("button4Menu");
                        }
                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "button3")
                        {
                            GameManager.userInterface.ShowWindow("button3Menu");
                            GameManager.userInterface.HideWindow("button2Menu");
                            GameManager.userInterface.HideWindow("button1Menu");
                            GameManager.userInterface.HideWindow("button4Menu");
                        }
                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "button4")
                        {
                            GameManager.userInterface.ShowWindow("button4Menu");
                            GameManager.userInterface.HideWindow("button2Menu");
                            GameManager.userInterface.HideWindow("button3Menu");
                            GameManager.userInterface.HideWindow("button1Menu");

                            GameManager.userInterface.CurrentSelectedButton.Selected = false;
                            GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.getButton("window4Button1", "button4Menu");
                            GameManager.userInterface.CurrentSelectedButton.Selected = true;
                        }

                        if (GameManager.userInterface.CurrentSelectedButton.ButtonName == "window4BackButton")
                        {

                            GameManager.userInterface.CurrentSelectedButton.Selected = false;
                            GameManager.userInterface.CurrentSelectedButton = GameManager.userInterface.getButton("button1", "rightMenu");
                            GameManager.userInterface.CurrentSelectedButton.Selected = true;
                            GameManager.userInterface.HideWindow("button4Menu");
                            GameManager.userInterface.HideWindow("button2Menu");
                            GameManager.userInterface.HideWindow("button3Menu");
                            GameManager.userInterface.HideWindow("button1Menu");
                        }
                        break;
                }
            }

            // Pause Menu for World and Dungeon 
            if (PressKey(Keys.P) && (GameState.currentGameState == GameStates.World || GameState.currentGameState == GameStates.Dungeon) && GlobalVariables.interacting == false)
            {
                GameState.setNextGameState(GameStates.WorldPauseMenu);
            }

            // Other Battle Commands

            // Save the last keyboard input
            previousKeyboardInput = keyboardInput;
        }

        private static bool PressKey(Keys key)
        {
            if (keyboardInput.IsKeyDown(key) && previousKeyboardInput.IsKeyUp(key))
                return true;
            return false;
        }

        private static void MoveCharacter(int direction, GameTime gameTime, Vector3 checkVector)
        {
            // Direction
            // 1 - Forward
            // 2 - Backward

            if (direction == 1)
            {
                Vector3 checkMove = new Vector3(
                        GameManager.characterManager.mainCharacterWorld.PositionX - checkVector.X * GlobalVariables.globalKeyboardMovementUnit,
                        GameManager.characterManager.mainCharacterWorld.PositionY,
                        GameManager.characterManager.mainCharacterWorld.PositionZ - checkVector.Z * GlobalVariables.globalKeyboardMovementUnit);

                BoundingSphere checkSphere = new BoundingSphere(checkMove, GlobalVariables.globalWorldCharacterRadius);

                if (GlobalVariables.characterAllowedToMove == false && numberOfFailedMoveAttempts > 10)
                {
                    checkMove = new Vector3(
                        GameManager.characterManager.mainCharacterWorld.PositionX - checkVector.X * (GlobalVariables.globalKeyboardMovementUnit * numberOfFailedMoveAttempts % 8),
                        GameManager.characterManager.mainCharacterWorld.PositionY,
                        GameManager.characterManager.mainCharacterWorld.PositionZ - checkVector.Z * (GlobalVariables.globalKeyboardMovementUnit * numberOfFailedMoveAttempts % 8));

                    checkSphere = new BoundingSphere(checkMove, GlobalVariables.globalWorldCharacterRadius);

                    if (checkSphere.Intersects(intersectingPlane) != PlaneIntersectionType.Intersecting)
                    {
                        GlobalVariables.characterAllowedToMove = true;
                        numberOfFailedMoveAttempts = 0;
                    }

                    if ((intersectingCharacter != null) &&
                        !checkSphere.Intersects(intersectingCharacter.getBoundingSphere))
                    {
                        GlobalVariables.characterAllowedToMove = true;
                        numberOfFailedMoveAttempts = 0;
                    }
                }

                if (numberOfFailedMoveAttempts > 15)
                    numberOfFailedMoveAttempts = 14;

                foreach (KeyValuePair<string, Plane> kvp in GlobalVariables.worldBoundaries)
                {
                    if (checkSphere.Intersects(kvp.Value) == PlaneIntersectionType.Intersecting)
                    {
                        GlobalVariables.characterAllowedToMove = false;
                        numberOfFailedMoveAttempts++;
                        intersectingPlane = kvp.Value;
                        break;
                    }
                }

                foreach (Character kvp in GameManager.characterManager.getListOfCharacters)
                {
                    if (checkSphere.Intersects(kvp.getBoundingSphere) && kvp != GameManager.characterManager.mainCharacterWorld)
                    {
                        GlobalVariables.characterAllowedToMove = false;
                        numberOfFailedMoveAttempts++;
                        intersectingCharacter = kvp;
                        break;
                    }
                }

                if (GlobalVariables.characterAllowedToMove == true)
                {
                    GameManager.characterManager.mainCharacterWorld.PositionX -= checkVector.X * GlobalVariables.globalKeyboardMovementUnit * GameManager.delta;
                    GameManager.characterManager.mainCharacterWorld.PositionZ -= checkVector.Z * GlobalVariables.globalKeyboardMovementUnit * GameManager.delta;

                    GameManager.characterManager.mainCharacterWorld.animationState = 2;
                }

            }
            else if (direction == 2)
            {
                Vector3 checkMove = new Vector3(
                        GameManager.characterManager.mainCharacterWorld.PositionX - checkVector.X * GlobalVariables.globalKeyboardMovementUnit,
                        GameManager.characterManager.mainCharacterWorld.PositionY,
                        GameManager.characterManager.mainCharacterWorld.PositionZ - checkVector.Z * GlobalVariables.globalKeyboardMovementUnit);

                BoundingSphere checkSphere = new BoundingSphere(checkMove, GlobalVariables.globalWorldCharacterRadius);

                if (GlobalVariables.characterAllowedToMove == false && numberOfFailedMoveAttempts > 8)
                {
                    checkMove = new Vector3(
                        GameManager.characterManager.mainCharacterWorld.PositionX + checkVector.X * (GlobalVariables.globalKeyboardMovementUnit * numberOfFailedMoveAttempts % 8),
                        GameManager.characterManager.mainCharacterWorld.PositionY,
                        GameManager.characterManager.mainCharacterWorld.PositionZ + checkVector.Z * (GlobalVariables.globalKeyboardMovementUnit * numberOfFailedMoveAttempts % 8));

                    checkSphere = new BoundingSphere(checkMove, GlobalVariables.globalWorldCharacterRadius);

                    if (checkSphere.Intersects(intersectingPlane) != PlaneIntersectionType.Intersecting)
                    {
                        GlobalVariables.characterAllowedToMove = true;
                        numberOfFailedMoveAttempts = 0;
                    }

                    if ((intersectingCharacter != null) &&
                        !checkSphere.Intersects(intersectingCharacter.getBoundingSphere))
                    {
                        GlobalVariables.characterAllowedToMove = true;
                        numberOfFailedMoveAttempts = 0;
                    }
                }

                if (numberOfFailedMoveAttempts > 12)
                    numberOfFailedMoveAttempts = 11;

                // Check World Boundaries
                foreach (KeyValuePair<string, Plane> kvp in GlobalVariables.worldBoundaries)
                {
                    if (checkSphere.Intersects(kvp.Value) == PlaneIntersectionType.Intersecting)
                    {
                        GlobalVariables.characterAllowedToMove = false;
                        numberOfFailedMoveAttempts++;
                        intersectingPlane = kvp.Value;
                        break;
                    }
                }

                // Check Character Boundaries
                foreach (Character kvp in GameManager.characterManager.getListOfCharacters)
                {
                    if (checkSphere.Intersects(kvp.getBoundingSphere) && kvp != GameManager.characterManager.mainCharacterWorld)
                    {
                        GlobalVariables.characterAllowedToMove = false;
                        numberOfFailedMoveAttempts++;
                        intersectingCharacter = kvp;
                        break;
                    }
                }

                if (GlobalVariables.characterAllowedToMove == true)
                {
                    GameManager.characterManager.mainCharacterWorld.PositionX += checkVector.X * GlobalVariables.globalKeyboardMovementUnit * GameManager.delta;
                    GameManager.characterManager.mainCharacterWorld.PositionZ += checkVector.Z * GlobalVariables.globalKeyboardMovementUnit * GameManager.delta;

                    GameManager.characterManager.mainCharacterWorld.animationState = 2;
                }
            }

            // Battle Transition
            if (intersectingCharacter != null)
            {
                if (intersectingCharacter.characterType == CharacterType.Enemy && GlobalVariables.drawBattle == false)
                {
                    GlobalVariables.encounteredCharacter = intersectingCharacter;
                    GlobalVariables.mainCharacterEncounterPosition = GameManager.characterManager.mainCharacterWorld.Location;
                    // This won't be used anymore since the new origin for the Battle is at (0, 0, 0)
                    /*
                    GlobalVariables.mainCharacterEncounterPositionMatrix = Matrix.CreateWorld(
                    new Vector3(
                        GlobalVariables.mainCharacterEncounterPosition.X - 3,
                        GlobalVariables.mainCharacterEncounterPosition.Y,
                        GlobalVariables.mainCharacterEncounterPosition.Z + 3),
                     
                    Vector3.Forward,
                    Vector3.Up);
                    */
                    GlobalVariables.flashScreenTrigger = true;
                    GameState.setNextGameState(GameStates.Battle);   // Transitioning
                    GameState.transitioning = true;
                }
            }
        }
    
    }
}

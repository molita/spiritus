﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spiritus
{
    public enum GameStates
    {
        MainMenu,
        World,
        Dungeon,
        Battle,
        Cutscene,
        WorldPauseMenu,
        BattlePauseMenu
    }

    public enum CharacterType
    {
        Enemy,
        Player,
        NPC
    }

    public enum ParticleType
    {
        World,
        Battle
    }

    public enum TranslationType
    {
        Linear,
        Cubic
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spiritus.DataStructures
{
    public class BattleGridPosition
    {
        /// <summary>
        /// A BattleGridPosition has a Left, Right, Up, and Down child 
        /// which is either null or a BattleGridPosition
        /// </summary>

        private BattleGridPosition Up;          // Corresponding BattleGridPosition when moving Up
        private BattleGridPosition Left;        // Corresponding BattleGridPosition when moving Left
        private BattleGridPosition Down;        // Corresponding BattleGridPosition when moving Left
        private BattleGridPosition Right;       // Corresponding BattleGridPosition when moving Right

        private byte position;
        private bool isEnemy;

        // Used for dynamically setting links

        private byte[,] PositionPriority;          // Array of priorities for BattleGridPositions when moving Up, Down, Left, and Right

        public byte[,] getPositionPriorityArray
        {
            get { return PositionPriority; }
        }

        public byte getGridPosition
        {
            get { return position; }
        }

        public bool isThisEnemy
        {
            get { return isEnemy; }
        }

        public BattleGridPosition getUpPosition
        {
            get { return Up; }
        }

        public BattleGridPosition getLeftPosition
        {
            get { return Left; }
        }

        public BattleGridPosition getDownPosition
        {
            get { return Down; }
        }

        public BattleGridPosition getRightPosition
        {
            get { return Right; }
        }

        public BattleGridPosition(byte location, bool enemy)
        {
            position = location;
            isEnemy = enemy;
        }

        public void setUpLocation(BattleGridPosition b)
        {
            Up = b;
        }

        public void setLeftLocation(BattleGridPosition b)
        {
            Left = b;
        }

        public void setDownLocation(BattleGridPosition b)
        {
            Down = b;
        }

        public void setRightLocation(BattleGridPosition b)
        {
            Right = b;
        }

        public void setLinkPrioritiesArray(byte[,] array)
        {
            PositionPriority = array;
        }

    }
}

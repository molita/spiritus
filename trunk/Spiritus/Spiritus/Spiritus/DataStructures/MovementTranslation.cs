﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Spiritus.DataStructures
{
    public class MovementTranslation
    {
        private Vector3 startPosition;      // Start Position
        private Vector3 endPosition;        // End Position
        private float speed;                // Speed to execute movement (value >= 1, smaller is faster)
        private float delay;                // Time in milliseconds to pause after movement has been executed before moving to next movement
        
        #region Properties
        // Properties
        public Vector3 getStartPosition
        {
            get { return startPosition; }
        }

        public Vector3 getEndPosition
        {
            get { return endPosition; }
        }

        public float getSpeed
        {
            get { return speed; }
        }
        #endregion

        public float getPostDelay
        {
            get { return delay; }
        }

        // Constructor
        public MovementTranslation(Vector3 start, Vector3 end, float movementSpeed, float postDelay)
        {
            // Set input variables
            startPosition = start;
            endPosition = end;
            speed = movementSpeed;
            delay = postDelay;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spiritus.GUI;
using System.Text.RegularExpressions;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Spiritus
{
    class Functions
    {
        public static bool PositionCheck(bool positionPolarity, float x, float y)
        {
            if (positionPolarity)
                return (x < y);
            else
                return (x > y);

        }
        // This function will take a String and wrap it to 
        // fit within a certain Line Length.
        public static string[] WrapText(String text, int lineLength)
        {
            string[] returnText;
            string originalText = text;
            int numberOfLines = 0;

            originalText = originalText.Trim();
            originalText = Regex.Replace(originalText, @"\s+", " ");

            // Determine the number of lines required for the body of text
            if (text.Length <= lineLength)
            {
                returnText = new string[1];
                returnText[0] = text;
            }
            else
            {
                numberOfLines = text.Length / lineLength;
                if (text.Length % lineLength > 0)
                    numberOfLines++;
                returnText = new String[numberOfLines];
            }

            // Delegate the body of text into a String Array
            for (int i = 0; i < numberOfLines; i++)
            {
                String newLine = "";
                if (originalText.Length <= lineLength + 1)
                {
                    newLine = originalText;
                    returnText[i] = newLine;
                    return returnText;
                }
                else
                    newLine = originalText.Substring(0, lineLength + 1);

                if (!newLine.EndsWith(" "))
                {
                    int lastSpace = newLine.LastIndexOf(" ");
                    newLine = newLine.Substring(0, lastSpace);
                }
                
                originalText = originalText.Substring(newLine.Length);
                if (originalText.StartsWith(" "))
                    originalText = originalText.Substring(1);
                returnText[i] = newLine;
            }

                return returnText;
        }

        public static float FlashScreen(GameTime gameTime, Color color, float speed)
        {
            GlobalVariables.flashScreenAlphaValue += speed * GameManager.delta;

            float alpha = (float)Math.Sin(GlobalVariables.flashScreenAlphaValue);          // Calculate the alpha value

            GlobalVariables.currentFlashColor = color * alpha;                             // Set the alpha value to the color

            return alpha;
        }

        public static void DrawFlashingScreen(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(
                    SpriteSortMode.BackToFront,
                    BlendState.AlphaBlend,
                    SamplerState.AnisotropicWrap,
                    DepthStencilState.DepthRead,
                    GlobalVariables.defaultRasterizer);

            spriteBatch.Draw(GameManager.textureManager.getTexture("FlashScreen", 2), GlobalVariables.rectangleFlashScreen, GlobalVariables.currentFlashColor);
            spriteBatch.End();
        }

        public static float CalculateY(float[,] height, Vector3 position)
        {
            Ray ray = new Ray();    // The Ray to check intersection of left or right plane
            float rayHeight = 100;  // The initial Height of the ray (arbitrarily large)
            float returnValue;      // The value to return

            int x = (int)position.X;
            int y = (int)position.Z;

            //y *= -1;
            y = Math.Abs(y);
            ray.Position = new Vector3(position.X, rayHeight, position.Z);        // Set the position of the Ray
            ray.Direction = new Vector3(0, -1, 0);              // Set the direction of the Ray

            // Square Vertices
            Vector3 topLeft = new Vector3(x, height[x, y + 1], -(y + 1));
            Vector3 topRight = new Vector3(x + 1, height[x + 1, y + 1], -(y + 1));
            Vector3 bottomRight = new Vector3(x + 1, height[x + 1, y], -y);
            Vector3 bottomLeft = new Vector3(x, height[x, y], -y);

            Plane rightPlane = new Plane(topRight, topLeft, bottomRight);
            Plane leftPlane = new Plane(topLeft, bottomLeft, bottomRight);

            // Set the point of intersection
            float distanceFromRightPlane = ray.Intersects(rightPlane).Value;
            float distanceFromLeftPlane = ray.Intersects(leftPlane).Value;

            float tempY = position.Y;

            if (distanceFromLeftPlane < distanceFromRightPlane)
            {
                tempY = rayHeight - distanceFromLeftPlane;
                if (GlobalVariables.DEBUG) Console.WriteLine("LEFT PLANE");
            }
            else if (distanceFromRightPlane <= distanceFromLeftPlane)
            {
                tempY = rayHeight - distanceFromRightPlane;
                if (GlobalVariables.DEBUG) Console.WriteLine("RIGHT PLANE");
            }

            // Set the Y co-ordinate by a linear interpolation
            returnValue = MathHelper.Lerp(position.Y, tempY, 0.38f);

            return returnValue;
        }
    }
}

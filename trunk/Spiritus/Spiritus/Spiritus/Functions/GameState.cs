﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Spiritus
{
    class GameState
    {
        // Game States
        // List of Game States:
        //
        // 1. Main Menu: This is at the start and end of the game
        // 2. World: When the character is on the island
        // 3. Dungeon: When the character is in the dungeon
        // 4. Battle: When the player is in a battle scene
        // 5. Cutscene?: When the player has no control over their character - run by dialogue and on-screen actions (not sure if this wil be implemented yet, but will give it a try)
        // 6. World / Pause Menu: Menu when player is in the world / dungeon
        // 7. Battle Pause Menu: Menu when player pauses during battle

        public static GameStates previousGameState = GameStates.MainMenu;
        public static GameStates currentGameState = GameStates.World;
        public static GameStates nextGameState = GameStates.World;
        // I believe this variable is to set all controls to be inactive while game is transitioning. I didn't write a comment initially, so unfortunately I don't exactly remember.
        public static bool transitioning = false;               
        

        public static void ChangeGameState(GameStates newGameState)
        {
            previousGameState = currentGameState;
            currentGameState = newGameState;
        }

        public static void setNextGameState(GameStates newGameState)
        {
            nextGameState = newGameState;
        }

        public static void updateGameState(GameTime gameTime, Game game)
        {
            if (currentGameState == nextGameState)
            {

            }
            else
            {
                // Transition - World to Battle
                if (currentGameState == GameStates.World && nextGameState == GameStates.Battle)
                {
                    GameManager.userInterface.UnloadGUIContent();
                    if (transitioning == false)
                    {
                        GameManager.userInterface.Loaded = false;
                        ChangeGameState(GameStates.Battle);
                    }
                }
                // Transition - Battle to World
                else if (currentGameState == GameStates.Battle && nextGameState == GameStates.World)
                {
                    GameManager.userInterface.UnloadGUIContent();
                    if (transitioning == false)
                    {
                        GameManager.userInterface.Loaded = false;
                        ChangeGameState(GameStates.World);
                    }
                }

                // Transition from World / Dungeon to In Game Menu
                else if (currentGameState == GameStates.World && nextGameState == GameStates.WorldPauseMenu)
                {
                    GameManager.userInterface.UnloadGUIContent();
                    GameManager.userInterface.Loaded = false;
                    ChangeGameState(GameStates.WorldPauseMenu);
                    transitioning = false;
                }
                else
                {
                    GameManager.userInterface.UnloadGUIContent();
                    GameManager.userInterface.Loaded = false;
                    ChangeGameState(nextGameState);
                    transitioning = false;
                }
            }
        }
    }
}

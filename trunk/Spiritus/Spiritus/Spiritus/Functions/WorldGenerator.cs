﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Spiritus
{
    class WorldGenerator
    {
        // Terrain Variables
        private VertexPositionNormalTexture[] terrain;
        private Texture2D heightmap;
        private GraphicsDevice graphics;
        private int[] terrainIndices;
        private int terrainWidth;
        private int terrainHeight;

        private float[,] heightData;
        private Color[] heightMapColors;

        private VertexBuffer terrainVertexBuffer;
        private IndexBuffer terrainIndexBuffer;

        private Texture2D waterTexture;
        private Texture2D grassTexture;
        private Texture2D mountainTexture;

        private string type;

        // Effect
        private Effect effect;

        public int getHeight
        {
            get { return terrainHeight; }
        }
        public int getWidth
        {
            get { return terrainWidth; }
        }
        public float[,] getHeightData
        {
            get { return heightData; }
        }
        public VertexPositionNormalTexture[] getVertices
        {
            get { return terrain; }
        }

        public Texture2D textureWater
        {
            set { waterTexture = value; }
            get { return waterTexture; }
        }

        public Texture2D textureGrass
        {
            set { grassTexture = value; }
            get { return grassTexture; }
        }

        public Texture2D textureMountain
        {
            set { mountainTexture = value; }
            get { return mountainTexture; }
        }
        public WorldGenerator(Texture2D height, ContentManager c, GraphicsDevice g, string t)
        {
            heightmap = height;
            graphics = g;
            effect = c.Load<Effect>("Effect1");
            type = t;
            if (type == "world")
                LoadWorldTextures();
            if (type == "battle")
                LoadWorldBattleTextures();
        }

        #region Terrain Generating Functions

        private void LoadHeightData(Texture2D heightMap)
        {
            terrainWidth = heightMap.Width;
            terrainHeight = heightMap.Height;

            heightMapColors = new Color[terrainWidth * terrainHeight];
            heightMap.GetData<Color>(heightMapColors);

            heightData = new float[terrainWidth, terrainHeight];
            for (int x = 0; x < terrainWidth; x++)
                for (int y = 0; y < terrainHeight; y++)
                    heightData[x, y] = heightMapColors[x + y * terrainWidth].R / 5.0f;
        }

        private void SetUpVertices(Texture2D heightMap)
        {
            terrainWidth = heightMap.Width;
            terrainHeight = heightMap.Height;

            terrain = new VertexPositionNormalTexture[terrainWidth * terrainHeight];

            for (int y = 0; y < terrainHeight; y++)
            {
                for (int x = 0; x < terrainWidth; x++)
                {
                    int idx = (y * terrainWidth) + x;
                    terrain[idx] = new
                        VertexPositionNormalTexture(
                        new Vector3(x, heightData[x, y], -y),
                        new Vector3(0.0f, 0.0f, 0.0f),
                        new Vector2(x, y));
                }
            }
        }

        private void SetUpIndices(Texture2D heightMap)
        {
            terrainWidth = heightMap.Width;
            terrainHeight = heightMap.Height;

            terrainIndices = new int[(terrainWidth - 1) * (terrainHeight - 1) * 6];
            int counter = 0;
            for (int y = 0; y < terrainHeight - 1; y++)
            {
                for (int x = 0; x < terrainWidth - 1; x++)
                {
                    int lowerLeft = x + y * terrainWidth;
                    int lowerRight = (x + 1) + y * terrainWidth;
                    int topLeft = x + (y + 1) * terrainWidth;
                    int topRight = (x + 1) + (y + 1) * terrainWidth;

                    terrainIndices[counter++] = topLeft;
                    terrainIndices[counter++] = lowerRight;
                    terrainIndices[counter++] = lowerLeft;
                    terrainIndices[counter++] = topLeft;
                    terrainIndices[counter++] = topRight;
                    terrainIndices[counter++] = lowerRight;
                }
            }
        }

        private void GenerateNormals()
        {
            for (int i = 0; i < terrain.Length / 3; i++)
            {
                Vector3 firstvec = terrain[i * 3 + 1].Position + terrain[i * 3].Position;
                Vector3 secondvec = terrain[i * 3].Position + terrain[i * 3 + 2].Position;
                Vector3 normal = Vector3.Cross(secondvec, firstvec);
                normal.Normalize();
                terrain[i * 3].Normal += normal;
                terrain[i * 3 + 1].Normal += normal;
                terrain[i * 3 + 2].Normal += normal;
            }

            foreach (var vertex in terrain)
                vertex.Normal.Normalize();
        }

        private void BufferTerrain()
        {
            terrainIndexBuffer = new IndexBuffer(
                graphics,
                IndexElementSize.ThirtyTwoBits,
                sizeof(int) * terrainIndices.Length,
                BufferUsage.WriteOnly
                );
            terrainIndexBuffer.SetData<int>(terrainIndices);

            terrainVertexBuffer = new VertexBuffer(
                graphics,
                VertexPositionNormalTexture.VertexDeclaration,
                terrain.Length,
                BufferUsage.WriteOnly
                );
            terrainVertexBuffer.SetData<VertexPositionNormalTexture>(terrain);
        }
        #endregion

        public void GenerateTerrain()
        {
            LoadHeightData(heightmap);
            SetUpVertices(heightmap);
            SetUpIndices(heightmap);
            GenerateNormals();
            BufferTerrain();
        }

        public void DrawWorld()
        {
            //if (type == "world")
                effect.Parameters["World"].SetValue(Matrix.Identity);
            //if (type == "battle")
                //effect.Parameters["World"].SetValue(GlobalVariables.mainCharacterEncounterPositionMatrix);
                //effect.Parameters["World"].SetValue(Matrix.Identity);
            effect.Parameters["View"].SetValue(Camera.ActiveCamera.View);
            effect.Parameters["Projection"].SetValue(Camera.ActiveCamera.Projection);

            
            effect.Parameters["HeightMap"].SetValue(heightmap);
            effect.Parameters["Mountain"].SetValue(mountainTexture);
            effect.Parameters["Water"].SetValue(waterTexture);
            effect.Parameters["Grass"].SetValue(grassTexture);
            
            effect.Parameters["LightDirection"].SetValue(new Vector3(5, 0, -20));
            effect.Parameters["LightColour"].SetValue(new Vector3(1.0f, 1.0f, 1.0f));
            effect.Parameters["AmbientColour"].SetValue(new Vector3(0.65f, 0.65f, 0.65f));
            effect.Parameters["Alpha"].SetValue(GlobalVariables.alpha100);

            effect.CurrentTechnique.Passes[0].Apply();

            DrawMesh();
        }

        private void DrawMesh()
        {
            // This Method draws ONLY the mesh
            // It requires the effect to be applied in a separate function

            // Set Vertices and Indices on Graphics Card
            graphics.SetVertexBuffer(terrainVertexBuffer);
            graphics.Indices = terrainIndexBuffer;

            // Draw World
            graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, terrain.Length, 0, terrainIndices.Length / 3);

        }

        // Pre-defined World Textures to load
        private void LoadWorldBattleTextures()
        {
            waterTexture = GameManager.textureManager.getTexture("DungeonTile", 0);
            grassTexture = GameManager.textureManager.getTexture("DungeonTile", 0);
            mountainTexture = GameManager.textureManager.getTexture("DungeonTile", 0);
        }

        private void LoadWorldTextures()
        {
            waterTexture = GameManager.textureManager.getTexture("Water", 0);
            grassTexture = GameManager.textureManager.getTexture("Grass", 0);
            mountainTexture = GameManager.textureManager.getTexture("Mountain", 0);
        }
    }
}

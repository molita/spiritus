﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Spiritus
{
    class Initialization
    {
        // Load all initial Variables and values into memory
        public static void InitializeGameManager(Game game, GraphicsDeviceManager graphicsDeviceManager)
        {
            graphicsDeviceManager.SynchronizeWithVerticalRetrace = true;
            game.IsFixedTimeStep = true;
            graphicsDeviceManager.PreferredBackBufferWidth = GlobalVariables.preferredWindowWidth;
            graphicsDeviceManager.PreferredBackBufferHeight = GlobalVariables.preferredWindowHeight;

            // Initialize Camera
            game.Components.Add(new Camera(game));
        }

        public static void InitializeGame(ContentManager content, Game game)
        {
            // Set Display Ratio
            GlobalVariables.displayRatio = (float)GameManager.graphicsDevice.Viewport.Width / (float)GameManager.graphicsDevice.Viewport.Height;
            // Set Rectangle for flashing screen (covers entire display)
            GlobalVariables.rectangleFlashScreen = new Rectangle(0, 0, GameManager.graphicsDevice.DisplayMode.Width, GameManager.graphicsDevice.DisplayMode.Height);

            // Set SpriteBatch for UI
            GlobalVariables.UserInterfaceSpriteBatch = new SpriteBatch(GameManager.graphicsDevice);
            GlobalVariables.ParticleSpriteBatch = new SpriteBatch(GameManager.graphicsDevice);
            GlobalVariables.dialogFont = content.Load<SpriteFont>("Fonts/DialogFont");
            GlobalVariables.menuFont = content.Load<SpriteFont>("Fonts/MenuFont");

            // Set Predefined States
            GlobalVariables.wireFrame = new RasterizerState();
            GlobalVariables.wireFrame.FillMode = FillMode.WireFrame;
            GlobalVariables.wireFrame.CullMode = CullMode.CullCounterClockwiseFace;

            GlobalVariables.defaultRasterizer = new RasterizerState();
            GlobalVariables.defaultRasterizer.FillMode = FillMode.Solid;
            GlobalVariables.defaultRasterizer.CullMode = CullMode.CullCounterClockwiseFace;

            GlobalVariables.defaultBlendState = new BlendState();
            GlobalVariables.defaultBlendState = BlendState.AlphaBlend;

            GlobalVariables.defaultDepthStencilState = new DepthStencilState();
            GlobalVariables.defaultDepthStencilState.DepthBufferEnable = true;
            GlobalVariables.defaultDepthStencilState.DepthBufferWriteEnable = true;

            // Set Dummy Variables
            GlobalVariables.dummyParticle = new ExtendedParticleEffect("DummyParticle", ParticleType.World, game, "Fireball");
            GlobalVariables.dummyIceLance = new DPSF.ParticleSystems.P_IceLance(game);

            GlobalVariables.CharacterDialog.dialog.LoadXML("GameData/dialog.xml");
        }

        public static void InitializeWorld()
        {
            DefineWorldBoundaries();
        }

        private static void DefineWorldBoundaries()
        {
            // Create the Planes that define World Boundaries
            Vector3 NorthWestCorner = new Vector3(0, 0, -GlobalVariables.worldHeight);
            Vector3 NorthEastCorner = new Vector3(GlobalVariables.worldWidth, 0, -GlobalVariables.worldHeight);
            Vector3 SouthWestCorner = new Vector3(0, 0, 0);
            Vector3 SouthEastCorner = new Vector3(GlobalVariables.worldWidth, 0, 0);

            Vector3 NorthY = new Vector3((GlobalVariables.worldWidth * 0.5f), 5, -GlobalVariables.worldHeight);
            Vector3 SouthY = new Vector3((GlobalVariables.worldWidth * 0.5f), 5, 0);
            Vector3 WestY = new Vector3(0, 5, -(GlobalVariables.worldHeight * 0.5f));
            Vector3 EastY = new Vector3(GlobalVariables.worldWidth, 5, -(GlobalVariables.worldHeight * 0.5f));

            Plane NorthWall = new Plane(NorthWestCorner, NorthY, NorthEastCorner);
            Plane SouthWall = new Plane(SouthWestCorner, SouthY, SouthEastCorner);
            Plane EastWall = new Plane(NorthEastCorner, EastY, SouthEastCorner);
            Plane WestWall = new Plane(NorthWestCorner, WestY, SouthWestCorner);

            GlobalVariables.worldBoundaries = new Dictionary<string, Plane>();

            GlobalVariables.worldBoundaries.Add("NorthWall", NorthWall);
            GlobalVariables.worldBoundaries.Add("SouthWall", SouthWall);
            GlobalVariables.worldBoundaries.Add("EastWall", EastWall);
            GlobalVariables.worldBoundaries.Add("WestWall", WestWall);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Spiritus.DataStructures;

namespace Spiritus
{
    public class InitiateBattleSpells
    {
        // Store outside Managers
        private ContentManager content;
        private BattleManager battleManager;
        private Game game;

        private int[] spellCounters = new int[2];             // To keep track of each spell's Counter
        private string[] spellNames = new string[2];          // To keep track of each spell's Name


        private Vector3 EnemyFrontOffset;// = new Vector3(-0.4f, 0.2f, 0);
        private Vector3 PlayerBodyOffset = new Vector3(0.0f, 0.2f, 0);
        // List of Spells
        // 0 - Fireball

        public InitiateBattleSpells(ContentManager c, BattleManager b, Game ga)
        {
            content = c;
            game = ga;
            battleManager = b;

            spellNames[0] = "Fireball";
            spellNames[1] = "Ice Lance";
        }

        // Fireball Spell
        public void CreateFireball(
            BattleCharacter origin,
            BattleCharacter target,
            float damage)
        {
            // Spell Number
            int spellNumber = 0;
            float spellCooldown = -1;        // The "cooldown" value of the spell

            // Create the offset for the Enemy
            // This offset is the direction towards the target. 
            // Essentially, the "front" of the enemy unit is a certain distance toward the target
            Vector3 frontVector = (Vector3.Normalize(target.Location - origin.Location));
            EnemyFrontOffset = (frontVector + new Vector3(0, 0.5f, 0)) * 0.3f;

            // Create Movement Translation Array
            MovementTranslation[,] movementArray = new MovementTranslation[1, 3];

            // Movement 1
            movementArray[0, 0] = new MovementTranslation(
                origin.Location + EnemyFrontOffset, // + new Vector3(0, 0.4f, 0),
                origin.Location + EnemyFrontOffset + new Vector3(0, 0.4f, 0),
                10,
                0);
            // Movement 2
            movementArray[0, 1] = new MovementTranslation(
                origin.Location + EnemyFrontOffset + new Vector3(0, 0.4f, 0),
                origin.Location + EnemyFrontOffset + new Vector3(0, 0.4f, 0) + new Vector3(-0.2f, -0.2f, 0),
                10,
                1000);
            // Movement 3
            movementArray[0, 2] = new MovementTranslation(
                origin.Location + EnemyFrontOffset + new Vector3(0, 0.4f, 0) + new Vector3(-0.2f, -0.2f, 0),
                target.Location + PlayerBodyOffset,
                20,
                0);
            
            // Create and Add particle to ParticleManager
            GameManager.particleManager.addNewBattleParticle(
                spellNames[spellNumber] + spellCounters[spellNumber],
                content,
                GlobalVariables.ParticleSpriteBatch,
                game,
                origin.Location + EnemyFrontOffset, "Fireball");
            
            // Create the Battle Particle Array with single particle
            ExtendedParticleEffect[] particleArray = new ExtendedParticleEffect[1];
            particleArray[0] = GameManager.particleManager.getBattleParticle(spellNames[spellNumber] + spellCounters[spellNumber]);
            // Create the Spell and add Spell to the list of Spells
            CreateSpellSingleTarget(
                0,
                origin,
                target,
                particleArray,
                movementArray,
                damage);

            origin.ApplySpellDelay(spellCooldown);
        }

        // Fireball Spell
        public void CreateIceLance(
            BattleCharacter origin,
            BattleCharacter target,
            float damage)
        {
            // Spell Number
            int spellNumber = 1;
            float spellCooldown = 1;        // The "cooldown" value of the spell
            // Create the offset for the Enemy
            // This offset is the direction towards the target. 
            // Essentially, the "front" of the enemy unit is a certain distance toward the target
            Vector3 frontVector = (Vector3.Normalize(target.Location - origin.Location));
            EnemyFrontOffset = (frontVector + new Vector3(0, 0.5f, 0)) * 0.3f;

            // Create Movement Translation Array
            MovementTranslation[,] movementArray = new MovementTranslation[1, 1];

            // Movement 1
            movementArray[0, 0] = new MovementTranslation(
                origin.Location + EnemyFrontOffset,
                target.Location + PlayerBodyOffset,
                50,
                0);

            // Create and Add particle to ParticleManager
            GameManager.particleManager.addNewBattleParticle(
                spellNames[spellNumber] + spellCounters[spellNumber],
                content,
                GlobalVariables.ParticleSpriteBatch,
                game,
                origin.Location + EnemyFrontOffset, "Ice Lance");

            // Create the Battle Particle Array with single particle
            ExtendedParticleEffect[] particleArray = new ExtendedParticleEffect[1];
            particleArray[0] = GameManager.particleManager.getBattleParticle(spellNames[spellNumber] + spellCounters[spellNumber]);
            // Create the Spell and add Spell to the list of Spells
            CreateSpellSingleTarget(
                1,
                origin,
                target,
                particleArray,
                movementArray,
                damage);

            origin.ApplySpellDelay(spellCooldown);
        }

        private void CreateSpellSingleTarget(
            int spellNumber,
            BattleCharacter originCharacter,
            BattleCharacter targetCharacter,
            ExtendedParticleEffect[] particle,
            MovementTranslation[,] movement,
            float hpModifier)
        {

            // Create the array for the single Target
            BattleCharacter[] singleTargetCharacter = new BattleCharacter[1];
            singleTargetCharacter[0] = targetCharacter;

            // Create the Battle Spell
            BattleSpell tempBattleSpell = new BattleSpell(
                spellNames[spellNumber] + spellCounters[spellNumber],
                originCharacter,
                singleTargetCharacter,
                movement,
                particle,
                hpModifier,
                battleManager.getListOfFloatingNumbers 
                );

            // Increment spell counter
            spellCounters[spellNumber]++;

            // Add Battle Spell to the list
            battleManager.getListOfBattleSpells.Add(tempBattleSpell);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
namespace Spiritus
{
    public class ParticleManager
    {
        SpriteBatch spriteBatch;

        // List of Particles
        //List<ExtendedParticleEffect> listOfParticleEffects = new List<ExtendedParticleEffect>();

        Effect effect;

        public ParticleManager(ContentManager content)
        {
            spriteBatch = new SpriteBatch(GameManager.graphicsDevice);

            effect = content.Load<Effect>("DPSFDefaultEffect");
        }

        public void addNewWorldParticle(string name, ContentManager c, SpriteBatch s, Game game, string particleName)
        {
            ExtendedParticleEffect newParticle = new ExtendedParticleEffect(name, ParticleType.World, game, particleName);

            // Initializes the Particle
            newParticle.particleSystem.AutoInitialize(GameManager.graphicsDevice, c, s);

            // Add the Particle to Lists of Particles
            GlobalVariables.listOfWorldSprites.Add(newParticle);
            //listOfParticleEffects.Add(newParticle);
        }

        public void addNewBattleParticle(string name, ContentManager c, SpriteBatch s, Game game, Vector3 OriginatingPosition, string particleName)
        {
            ExtendedParticleEffect newParticle = new ExtendedParticleEffect(name, ParticleType.Battle, game, particleName);

            // Initializes the Particle
            newParticle.particleSystem.AutoInitialize(GameManager.graphicsDevice, c, s);
            // Set the initial position of the Particle
            newParticle.particleSystem.Emitter.PositionData.Position = OriginatingPosition;        

            // Add the Particle to Lists of Particles
            GlobalVariables.listOfBattleSprites.Add(newParticle);
            //listOfParticleEffects.Add(newParticle);
        }

        public ExtendedParticleEffect getBattleParticle(string name)
        {
            ExtendedParticleEffect returnedExtendedParticleEffect = null;       // Temporary variable to hold the particle effect

            // Continue going through list of particles until the match is found
            for (int i = 0; i < GlobalVariables.listOfBattleSprites.Count; i++)
            {
                // Check if the sprite is a Particle
                if (GlobalVariables.listOfBattleSprites[i].GetType() == GlobalVariables.dummyParticle.GetType())
                {
                    ExtendedParticleEffect tempExtendedParticleEffect = (ExtendedParticleEffect)GlobalVariables.listOfBattleSprites[i];
                    
                    // Check if the name of the particle matches
                    if (tempExtendedParticleEffect.getParticleName == name)
                    {
                        returnedExtendedParticleEffect = tempExtendedParticleEffect;    // Set the return particle effect to the match
                        break;  // Break out of loop
                    }
                }
            }

            if (returnedExtendedParticleEffect == null)
                Console.WriteLine("WARNING! Particle " + name + " was not found in Battle Sprites!");

            return returnedExtendedParticleEffect;
        }

        public void removeWorldParticle(string name)
        {
            ExtendedParticleEffect removeParticle = null;

            bool success = false;

            foreach (GlobalVariables.ListOfSprites sprite in GlobalVariables.listOfWorldSprites)
            {
                if (sprite.GetType() == GlobalVariables.dummyParticle.GetType())
                {
                    // This cast is safe since we're ensuring that this is a particle with the above check
                    removeParticle = (ExtendedParticleEffect)sprite;

                    if (removeParticle.getParticleName == name)
                    {
                        // If the particleName matches, remove it from both lists
                        //listOfParticleEffects.Remove(removeParticle);
                        GlobalVariables.listOfWorldSprites.Remove(removeParticle);
                        success = true;
                        break;
                    }
                }
            }

            if (!success)
                Console.WriteLine("WARNING: COULD NOT FIND PARTICLE " + name + " TO REMOVE.");
        }
        /*
         * These may not be needed
         * 
        public void removeBattleParticle(string name)
        {
            ExtendedParticleEffect removeParticle = null;

            bool success = false;

            foreach (GlobalVariables.ListOfSprites sprite in GlobalVariables.listOfBattleSprites)
            {
                if (sprite.GetType() == GlobalVariables.dummyParticle.GetType())
                {
                    // This cast is safe since we're ensuring that this is a particle with the above check
                    removeParticle = (ExtendedParticleEffect)sprite;

                    if (removeParticle.getParticleName == name)
                    {
                        // If the particleName matches, remove it from both lists
                        listOfParticleEffects.Remove(removeParticle);
                        GlobalVariables.listOfBattleSprites.Remove(removeParticle);
                        success = true;
                        break;
                    }
                }
            }

            if (!success)
                Console.WriteLine("WARNING: COULD NOT FIND PARTICLE " + name + " TO REMOVE.");
        }

        public void RemoveInactiveParticles(GameTime gameTime)
        {
            for (int i = listOfParticleEffects.Count - 1; i >= 0; i--)
            {
                ExtendedParticleEffect tempParticleEffect = listOfParticleEffects[i];
                // Check if the particle is flagged to be removed, then remove it
                if (tempParticleEffect.isParticleComplete)
                {
                    // Check if the particle is Battle or World, then run the corresponding function to remove it
                    if (tempParticleEffect.getParticleType == ParticleType.Battle)
                        removeBattleParticle(tempParticleEffect.getParticleName);
                    else if (tempParticleEffect.getParticleType == ParticleType.World)
                        removeWorldParticle(tempParticleEffect.getParticleName);
                    else
                        Console.WriteLine("WARNING! Invalid Particle Type for: " + tempParticleEffect.getParticleName + ". Type: " + tempParticleEffect.getParticleType);
                }
            }
        }
        */
        // This function will translate one starting 3D coordinate 
        // to a desination 3D coordinate based on a specified speed
        // Input: Vector3 startPosition, Vector3 endPosition
        // Output: void

        public void Update(GameTime gameTime)
        {
            //RemoveInactiveParticles(gameTime);      // Remove any Inactive Particles
        }

        public void ClearParticles()
        {
            //listOfParticleEffects.Clear();
        }

    }
}

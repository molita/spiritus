﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using DPSF.ParticleSystems;
using DPSF;
using Spiritus.GameObjects;
namespace Spiritus
{
    // Note For Myself in case I forget later
    // I wrote this to extend the current particle class so I could add my own movement functions
    public class ExtendedParticleEffect : WorldObject, GlobalVariables.ListOfSprites, IComparable
    {
        private string particleName;
        public DefaultSprite3DBillboardParticleSystem particleSystem;

        // Variables for Particle Movement
        private bool movingParticle = false;
        private bool newParticleMovement = true;
        public float newPositionX, newPositionY, newPositionZ, xInterval, yInterval, zInterval;
        private float distanceFromCamera;

        public double tempGameTime = 0;
        public double movementSmoothInterval = 30;
        public bool resetGameTime = true;

        public bool positionXPolarity, positionYPolarity, positionZPolarity;        // Polarity for doing particle translations

        private double fadeOutTime = 1000;          // Time for particle to completely fade out
        private double fadeOutTimeTimer = 0;        // Timer value to keep track of how much time passed for fade out
        private bool particleComplete = false;      // Flag to say that the particle is no longer needed and should be terminated
        private bool fadeOut = false;               // Flag to start the fade out for the Particle
        private bool startedRemoval = false;        // Flag to trigger the start of removing the particle

        private ParticleType particleType;          // The type of Particle (World or Battle)

        private Vector3 intervalVector = new Vector3();

        public bool isParticleComplete
        {
            get { return particleComplete; } 
        }

        public ParticleType getParticleType
        {
            get { return particleType; }
        }

        public string getParticleName
        {
            get { return particleName; }
        }

        public Vector3 getPosition
        {
            get { return particleSystem.Emitter.PositionData.Position; }
        }

        public bool isParticleMoving
        {
            set { movingParticle = value; }
            get { return movingParticle; }
        }

        public bool readyForNewMovement
        {
            set { newParticleMovement = value; }
            get { return newParticleMovement; }
        }

        public bool getFadingStatus
        {
            get { return fadeOut; }
        }
        /*
        public int CompareTo(object obj)
        {
            // Object is this type
            if (obj.GetType() == this.GetType())
            {
                ExtendedParticleEffect otherParticle = obj as ExtendedParticleEffect;
                if (otherParticle != null)
                    return getDistanceFromCamera.CompareTo(otherParticle.getDistanceFromCamera);
                else
                    throw new ArgumentException("Object is not a Particle");
            }
            // Object is a character 
            else
            {
                Character otherCharacter = obj as Character;
                if (otherCharacter != null)
                    return getDistanceFromCamera.CompareTo(otherCharacter.getDistanceFromCamera);
                else
                    throw new ArgumentException("Object is not a Character");
            }

        }
        */
        public ExtendedParticleEffect(string name, ParticleType type, Game game, string pName): base(name, new Vector3(0, 0, 0), 0f, 0, 0)
        {
            particleName = name;    // Set the name
            particleType = type;

            switch (pName)
            {
                case "Fireball":
                    particleSystem = new P_Fireball(game);
                    break;
                case "Ice Lance":
                    particleSystem = new P_IceLance(game);
                    break;
            }

        }

        public void FadeOutParticle()
        {
            // This gives a fade out effect on the particle by setting the size to an extremely small value
            // The particle will then have to be removed after a few seconds

            particleSystem.InitialProperties.StartSizeMin = 0.001f;

            particleSystem.InitialProperties.StartSizeMax =
                particleSystem.InitialProperties.EndSizeMin =
                particleSystem.InitialProperties.EndSizeMax =
                particleSystem.InitialProperties.StartSizeMin;

            fadeOut = true;
        }

        public void UpdateExtended(GameTime gameTime)
        {
            // Update the base 
            particleSystem.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
            // Update distance from Camera
            // This is required to sort the sprites in drawing order
            distanceFromCamera = Vector3.Distance(particleSystem.Emitter.PositionData.Position, Camera.ActiveCamera.Position);
            // Set World View Projection Matrices
            particleSystem.SetWorldViewProjectionMatrices(Matrix.Identity, Camera.ActiveCamera.View, Camera.ActiveCamera.Projection);


            // Check if the particle is set to fade out
            if (fadeOut)
                RemoveParticleAfterSetTime(fadeOutTime, gameTime);  // Remove the particle after a set time
        }

        private void RemoveParticleAfterSetTime(double milliseconds, GameTime gameTime)
        {
            // Check if Removal of Particle has started
            if (!startedRemoval)
            {
                startedRemoval = true;      // Turn flag on
            }
            else
            {
                fadeOutTimeTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
                // Check if the given milliseconds interval has expired
                if (fadeOutTimeTimer > fadeOutTime)
                    particleComplete = true;    // Set the flag to remove particle

            }
        }

        public void Translate3DLocation(Vector3 startPosition, Vector3 endPosition, float speed, GameTime gameTime)
        {
            if (!isParticleMoving && readyForNewMovement)
            {
                isParticleMoving = true;

                newPositionX = endPosition.X;
                newPositionY = endPosition.Y;
                newPositionZ = endPosition.Z;

                xInterval = (newPositionX - startPosition.X) / (speed);
                yInterval = (newPositionY - startPosition.Y) / (speed);
                zInterval = (newPositionZ - startPosition.Z) / (speed);

                // Prevent the intervals from being too small
                // Not sure if I really need this, it causes problems when the speed is too high (ie. very slow speed)
                /*if ((xInterval < 0.001) && (xInterval > 0))
                    xInterval = 0.001f;
                if ((xInterval > -0.001) && (xInterval < 0))
                    xInterval = -0.001f;
                if ((yInterval < 0.001) && (yInterval > 0))
                    yInterval = 0.001f;
                if ((yInterval > -0.001) && (yInterval < 0))
                    yInterval = -0.001f;
                if ((zInterval < 0.001) && (zInterval > 0))
                    zInterval = 0.001f;
                if ((zInterval > -0.001) && (zInterval < 0))
                    zInterval = -0.001f;
                */
                intervalVector = new Vector3(xInterval, yInterval, zInterval);

                if (startPosition.X < newPositionX)
                    positionXPolarity = true;
                else
                    positionXPolarity = false;

                if (startPosition.Y < newPositionY)
                    positionYPolarity = true;
                else
                    positionYPolarity = false;

                if (startPosition.Z < newPositionZ)
                    positionZPolarity = true;
                else
                    positionZPolarity = false;
            }

            if (Functions.PositionCheck(positionXPolarity, particleSystem.Emitter.PositionData.Position.X, newPositionX) ||
                Functions.PositionCheck(positionYPolarity, particleSystem.Emitter.PositionData.Position.Y, newPositionY) ||
                Functions.PositionCheck(positionZPolarity, particleSystem.Emitter.PositionData.Position.Z, newPositionZ))
            {
                tempGameTime += gameTime.ElapsedGameTime.TotalMilliseconds;

                if (tempGameTime > movementSmoothInterval)
                {
                    particleSystem.Emitter.PositionData.Position = particleSystem.Emitter.PositionData.Position + intervalVector;
                    tempGameTime = 0;
                }
            }
            else
            {
                isParticleMoving = false;
                if (GlobalVariables.DEBUG)
                    //removeBattleParticle("Fireball");
                    if (readyForNewMovement)
                    {
                        readyForNewMovement = false;
                    }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            UpdateExtended(gameTime);
            particleSystem.Draw();
        }
    }
}


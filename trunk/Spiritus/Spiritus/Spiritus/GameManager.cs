using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using DPSF;
using DPSF.ParticleSystems;

using Nuclex.Fonts;

using Spiritus.GameObjects;

namespace Spiritus
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameManager : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphicsDeviceManager;
        public static GraphicsDevice graphicsDevice;
        SpriteBatch spriteBatch;
        Effect effect, characterEffect;

        public static UserInterface userInterface;
        
        // User Built Managers
        public static CharacterManager characterManager;
        public static TextureManager textureManager;
        public static ParticleManager particleManager;
        public static BattleManager battleManager;

        private List<BattleManager> listOfBattleManagers = new List<BattleManager>();

        bool createdBattleSprites = false;

        // TEMPORARY VARIABLE FOR BATTLE MANAGER
        bool loadedNewBattle = false;

        public static bool firstBattleFlashDone = false;
        public static bool secondBattleFlashDone = false;

        public static float delta; // Global delta
        #region Terrain Generation Variables

        WorldGenerator world;
        WorldGenerator battleWorld;

        #endregion
        
        public GameManager()
        {
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            Initialization.InitializeGameManager(this, graphicsDeviceManager);

            Content.RootDirectory = "Content";

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Initialize User Interface
            graphicsDevice = GraphicsDevice;
            userInterface = new UserInterface();
            characterManager = new CharacterManager();
            particleManager = new ParticleManager(Content);


            Initialization.InitializeGame(Content, this);

            base.Initialize();
        }
        
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(graphicsDevice);

            // TODO: use this.Content to load your game content here

            // Texture Manager
            textureManager = new TextureManager();
            textureManager.LoadKeyTextures(this);

            //flashScreenTexture = Content.Load<Texture2D>("FlashScreen");
            characterEffect = Content.Load<Effect>("CharacterEffects");
            effect = Content.Load<Effect>("Effect1");

            // Fonts
            GlobalVariables.arialVectorFont = Content.Load<VectorFont>("Fonts/Arial");

            // Character Manager
            characterManager.InitializeCoreCharacters(Content);
            characterManager.InitializeCoreMainCharacter(Content);

            #region World Generators
            // Main World 
            world = new WorldGenerator(textureManager.getTexture("newheightmap", 0), Content, graphicsDevice, "world");
            world.GenerateTerrain();
            GlobalVariables.currentWorld = world;

            // Battle Scene
            battleWorld = new WorldGenerator(textureManager.getTexture("Heightmaps/BattleSceneHeightmap", 0), Content, graphicsDevice, "battle");
            battleWorld.GenerateTerrain();
            #endregion

            // Set some world variables
            GlobalVariables.worldHeight = world.getHeight;
            GlobalVariables.worldWidth = world.getWidth;

            foreach (Character c in characterManager.getListOfCharacters)
            {
                GlobalVariables.listOfWorldSprites.Add(c);
            }

            Initialization.InitializeWorld();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            delta = (float)gameTime.ElapsedGameTime.TotalMilliseconds;      // Update global Delta

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            KeyboardState keyboard = Keyboard.GetState();

            if (keyboard.IsKeyDown(Keys.N))
            {
                GlobalVariables.CharacterDialog.dialog.JumpToNextDialog("Haruhi3");
                Console.WriteLine("CURRENT GAME STATE: " + GameState.currentGameState);
                Console.WriteLine("TRANSITIONING: " + GameState.transitioning);
            }

            // To Toggle Fullscreen
            if (keyboard.IsKeyDown(Keys.F11))
                graphicsDeviceManager.ToggleFullScreen();

            if (keyboard.IsKeyDown(Keys.Escape))
                Exit();

            if (keyboard.IsKeyDown(Keys.X))
                GlobalVariables.DEBUG = true;
            else
                GlobalVariables.DEBUG = false;

            if (GameState.currentGameState == GameStates.Battle)
            {
                if (!loadedNewBattle)
                {
                    battleManager = new BattleManager(GlobalVariables.encounteredCharacter, gameTime, this);
                    //listOfBattleManagers.Add(new BattleManager(GlobalVariables.encounteredCharacter, gameTime, this));
                    loadedNewBattle = true;
                }
                
            }

            if (keyboard.IsKeyDown(Keys.D1))
                Camera.ActiveCamera.ChangeBattleCameraPosition(1);
            if (keyboard.IsKeyDown(Keys.D2))
                Camera.ActiveCamera.ChangeBattleCameraPosition(2);
            if (keyboard.IsKeyDown(Keys.D3))
                Camera.ActiveCamera.ChangeBattleCameraPosition(3);
            if (keyboard.IsKeyDown(Keys.D4))
                Camera.ActiveCamera.ChangeBattleCameraPosition(4);
            if (keyboard.IsKeyDown(Keys.D5))
                Camera.ActiveCamera.ChangeBattleCameraPosition(5);
            if (keyboard.IsKeyDown(Keys.D6))
                Camera.ActiveCamera.ChangeBattleCameraPosition(6);
            if (keyboard.IsKeyDown(Keys.D7))
                Camera.ActiveCamera.ChangeBattleCameraPosition(7);
            if (keyboard.IsKeyDown(Keys.D8))
                Camera.ActiveCamera.ChangeBattleCameraPosition(8);
            if (keyboard.IsKeyDown(Keys.D9))
                Camera.ActiveCamera.ChangeBattleCameraPosition(9);
            // TODO: Add your update logic here

            
            if (loadedNewBattle)
                GameInput.gameInput(gameTime, battleManager);
            else
                GameInput.gameInput(gameTime, null);
            
            Camera.ActiveCamera.MainCharacterCamera(
                characterManager.mainCharacterWorld.Location);
                // Commenting this out because it seems like it's not needed.
                // Tried changing the value to 20, 80 etc and nothing seemed to change.
                //characterManager.mainCharacterWorld.getAngleX);
            
            if (GlobalVariables.flashScreenTrigger)
                FlashScreenBattleEncounter(gameTime);

            if (firstBattleFlashDone)
            {
                GlobalVariables.drawWorld = false;
                GlobalVariables.drawBattle = true;
                
                GameState.transitioning = false;
            }

            // Test to end battle
            if (keyboard.IsKeyDown(Keys.E) && GlobalVariables.drawBattle == true)
            {
                GlobalVariables.drawBattle = false;
                GlobalVariables.drawWorld = true;

                GameState.setNextGameState(GameStates.World);
                loadedNewBattle = false;
                
                GlobalVariables.flashScreenTrigger = false;
                GlobalVariables.flashScreenAlphaValue = 0;
                firstBattleFlashDone = false;
                secondBattleFlashDone = false;

                characterManager.removeWorldCharacter(GlobalVariables.encounteredCharacter);
                GameInput.intersectingCharacter = null;
                GlobalVariables.characterAllowedToMove = true;

                battleManager.ClearBattleManager();
                createdBattleSprites = false;

                Console.WriteLine();
            }

            // This is to prevent the main character from being part of the character interaction check.
            characterManager.CheckInteractingCharacter();

            // This is for updating the Game State
            GameState.updateGameState(gameTime, this);
            // Needed to update the User Interface
            userInterface.Update(gameTime);

            particleManager.Update(gameTime);

            base.Update(gameTime);
        }

        private void FlashScreenBattleEncounter(GameTime gameTime)
        {
            // Check if the first battle flash isn't done yet
            if (!firstBattleFlashDone)
            {
                // Check if it's done the fade
                if (Functions.FlashScreen(gameTime, Color.Red, 0.005f) <= 0)      // Execute first Battle Flash
                {
                    GlobalVariables.flashScreenAlphaValue = 1.67f;      // Reset the flash screen coefficient 
                    firstBattleFlashDone = true;                        // Set the second Battle Flash to start
                }
            }
            // Check if second battle flash is not done yet, and first battle flash is done
            if (!secondBattleFlashDone && firstBattleFlashDone)
            {
                if (Functions.FlashScreen(gameTime, Color.Red, 0.0007f) <= 0)
                {
                    secondBattleFlashDone = true;                       // Execute second Battle Flash
                    GlobalVariables.flashScreenTrigger = false;         // Stop the screen flash trigger
                }
            }
        }

        private void DrawSprites(GameTime gameTime)
        {
            // Checks if the current item is a Character / Battle Character

            // World Sprites
            if (GameState.currentGameState == GameStates.World || GameState.currentGameState == GameStates.WorldPauseMenu)
            {
                // Ensures that world sprites is not null
                if (GlobalVariables.listOfWorldSprites != null)
                {
                    // Sort Sprites by distance from camera
                    GlobalVariables.listOfWorldSprites.Sort();

                    for (int i = GlobalVariables.listOfWorldSprites.Count - 1; i >= 0; i--)
                    {
                        spriteBatch.Begin(
                                SpriteSortMode.BackToFront,
                                BlendState.AlphaBlend,
                                SamplerState.AnisotropicWrap,
                                DepthStencilState.DepthRead,
                                GlobalVariables.defaultRasterizer);

                        WorldObject tempObjectWorld = (WorldObject)GlobalVariables.listOfWorldSprites[i];
                        tempObjectWorld.Draw(gameTime);

                        spriteBatch.End();
                    }
                    /*
                    for (int i = GlobalVariables.listOfWorldSprites.Count - 1; i >= 0; i--)
                    {
                        spriteBatch.Begin(
                            SpriteSortMode.BackToFront,
                            BlendState.AlphaBlend,
                            SamplerState.AnisotropicWrap,
                            DepthStencilState.DepthRead,
                            GlobalVariables.defaultRasterizer);
                        // Checks if the sprite is a Character
                        if (GlobalVariables.listOfWorldSprites[i].GetType() == characterManager.getListOfCharacters[0].GetType())
                        {
                            Character tempChar = (Character)GlobalVariables.listOfWorldSprites[i];
                            tempChar.SetY(
                                GlobalVariables.currentWorld.getHeightData,
                                tempChar.Location);
                            /*tempChar.CalculateY(
                                GlobalVariables.currentWorld.getHeightData,
                                tempChar.Position);
                            tempChar.CharacterAnimation(tempChar.animationState, gameTime);
                            tempChar.Draw(gameTime);
                        }
                        // Otherwise, the sprite is a particle
                        else
                        {
                            ExtendedParticleEffect tempChar = (ExtendedParticleEffect)GlobalVariables.listOfWorldSprites[i];
                            tempChar.UpdateExtended(gameTime);
                            tempChar.particleSystem.Draw();
                        }
                        spriteBatch.End();
                    } */
                }
            }
            // For Battle Sprites
            else if (GameState.currentGameState == GameStates.Battle)
            {
                // To sort the Sprites
                if (GlobalVariables.listOfBattleSprites != null)
                {
                    GlobalVariables.listOfBattleSprites.Sort();

                    // Ensures that the list of Battle Sprites is ready to be populated
                    if (!createdBattleSprites && battleManager != null && characterManager.getListOfBattleCharacters != null && battleManager.getBattleLoadedStatus)
                    {
                        // Sets the flag that battle sprites are created
                        createdBattleSprites = true;
                    }
                    // Draw the sprites once they're ready to be populated
                    if (createdBattleSprites)
                    {
                        for (int i = GlobalVariables.listOfBattleSprites.Count - 1; i >= 0; i--)
                        {

                            spriteBatch.Begin(
                                    SpriteSortMode.BackToFront,
                                    BlendState.AlphaBlend,
                                    SamplerState.AnisotropicWrap,
                                    DepthStencilState.DepthRead,
                                    GlobalVariables.defaultRasterizer);

                            WorldObject tempObject = (WorldObject)GlobalVariables.listOfBattleSprites[i];
                            tempObject.Draw(gameTime);

                            spriteBatch.End();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // Set States
            
            GraphicsDevice.DepthStencilState = GlobalVariables.defaultDepthStencilState;
            GraphicsDevice.RasterizerState = GlobalVariables.defaultRasterizer;
            GraphicsDevice.BlendState = GlobalVariables.defaultBlendState;

            if (GlobalVariables.drawWorld)
                world.DrawWorld();
            if (GlobalVariables.drawBattle)
                battleWorld.DrawWorld();

            // This is to draw the flashing rectangle on the terrain, but sprites are still on top.
            // This is to give the desired effect for the battle transition
            if (GlobalVariables.flashScreenTrigger && !firstBattleFlashDone)
                Functions.DrawFlashingScreen(spriteBatch);

            if (userInterface.Loaded == false)
                userInterface.LoadGUIContent(Content);

            // This draws all the sprites on the screen
            DrawSprites(gameTime);

            // Some of these have to be drawn before the User Interface
            if (loadedNewBattle)
                battleManager.Update();

            userInterface.Draw(gameTime);
            // This has to be drawn after user interface
            if (loadedNewBattle && BattleManager.readyForUI)
                userInterface.DrawBattleBottomMenuStats(battleManager.getListOfPlayerBattleCharacters);

            // This is to draw the flashing rectangle on top of everything
            if (GlobalVariables.flashScreenTrigger && !secondBattleFlashDone && firstBattleFlashDone)
                Functions.DrawFlashingScreen(spriteBatch);

            base.Draw(gameTime);
        }
    }
}

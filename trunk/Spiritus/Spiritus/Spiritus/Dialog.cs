﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Spiritus
{
    class Dialog
    {
        // FOR DEBUGGING
        private string debugMessage;

        private XmlDocument xmlDoc;
        private XmlNodeList xmlNode;
        
        private string currentDialog = "seq1";
        private int currentDialogSequenceNumber = 1;
        private string nextDialog = "seq2";

        private bool resetDialog;

        // Loads the dialog XML into memory
        // Initially written to load entire dialog into memory. 
        // If size is an issue then this will have to be modified
        public void LoadXML(string filename)
        {
            xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);
            PopulateDictionary();
        }

        private void PopulateDictionary()
        {
            xmlNode = xmlDoc.GetElementsByTagName("Characters");
            foreach (XmlNode nodeCharacters in xmlNode)
            {
               // Iterate through Characters
               foreach (XmlNode nodeCharacterName in nodeCharacters)
               {
                   GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialog.Add(nodeCharacterName.Name, "seq1");
                   GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber.Add(nodeCharacterName.Name, 1);
                   GlobalVariables.CharacterDialog.DictionaryCharacterNextDialog.Add(nodeCharacterName.Name, "seq2");
                   // This variable contains the first line in the current Dialog sequence
                   GlobalVariables.CharacterDialog.DictionaryCharacterFirstSequenceDialogs.Add(nodeCharacterName.Name, "This should not be displayed! This is a bug!");
               }
            }
        }

        public void JumpToNextDialog(string characterName) 
        {
            GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialog[characterName] = GlobalVariables.CharacterDialog.DictionaryCharacterNextDialog[characterName];
            GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber[characterName] = 1;
            GlobalVariables.CharacterDialog.DictionaryCharacterFirstSequenceDialogs[characterName] = "This should not be displayed! This is a bug!";

            Console.WriteLine("Current Dialog: " + GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialog[characterName]);
        }

        public void resetCurrentDialog()
        {
            resetDialog = true;
        }

        public string LoadDialog(string characterName)
        {
            xmlNode = xmlDoc.GetElementsByTagName("Characters");        // There should only be one!
            debugMessage = "";
            // Try Catch statement to catch if the character could not be found.
            try{
                currentDialog = GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialog[characterName];
            }
            catch{
                Console.Error.WriteLine("ERROR: COULD NOT FIND CHARACTER:" + characterName);
                return "ERROR: COULD NOT FIND CHARACTER";
            }
            // Resets the Dialog sequence 
            // This is used for when the player interrupts the conversation. 
            // The next time the player talks to the NPC the conversation is reset.
            if (resetDialog)
            {
                GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber[characterName] = 1;
                resetDialog = false;
            }

            currentDialogSequenceNumber = GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber[characterName];

            debugMessage = "[currentDialogsequenceNumber: " + currentDialogSequenceNumber + 
                "] [currentDialog: " + currentDialog +
                "] [nextDialog: " + nextDialog;

            foreach (XmlNode nodeCharacters in xmlNode)
            {
                // Iterate through Characters
                foreach (XmlNode nodeCharacterName in nodeCharacters)
                {
                    // Checks if desired Character is found
                    if (nodeCharacterName.Name == characterName)
                    {
                        // Iterate through the Dialog Sequences
                        foreach (XmlNode nodeDialogNumber in nodeCharacterName)
                        {
                            // Checks if desired Dialog Sequence is found
                            if (nodeDialogNumber.Name == currentDialog)
                            {
                                // Obtain the desired Dialog Sequence number String
                                foreach (XmlNode nodeDialogSequenceNumber in nodeDialogNumber)
                                {
                                    // Check if desired Dialog Sequence Number is found
                                    if (nodeDialogSequenceNumber.Name == "s" + currentDialogSequenceNumber)
                                    {
                                        // Sets the first string for the sequence
                                        if (nodeDialogSequenceNumber.Name == "s1")
                                            GlobalVariables.CharacterDialog.DictionaryCharacterFirstSequenceDialogs[characterName] = nodeDialogSequenceNumber.InnerText;
                                        GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber[characterName]++;
                                        debugMessage = "s1";
                                        if (nodeDialogSequenceNumber.Attributes.Count > 0)
                                        {
                                            foreach (XmlAttribute xmlAttribute in nodeDialogSequenceNumber.Attributes)
                                            {
                                                if (xmlAttribute.Name == "portrait")
                                                {
                                                    if (xmlAttribute.Value == Convert.ToString(2))
                                                    {
                                                        Console.WriteLine("THIS WORKS! This will be useful for special cases for specific lines. I have the idea of having the character portraits changing during dialogue.");
                                                    }
                                                }
                                            }
                                        }
                                        return nodeDialogSequenceNumber.InnerText;
                                    }
                                    if (nodeDialogSequenceNumber.Name == "next")
                                    {
                                        GlobalVariables.CharacterDialog.DictionaryCharacterNextDialog[characterName] = nodeDialogSequenceNumber.InnerText;
                                        GlobalVariables.CharacterDialog.DictionaryCharacterCurrentDialogSequenceNumber[characterName] = 1;
                                        GlobalVariables.interacting = false;
                                        JumpToNextDialog(characterName);
                                        debugMessage = "nextDialog: " + GlobalVariables.CharacterDialog.DictionaryCharacterNextDialog[characterName] + " currentDialogSequenceNumber: " + currentDialogSequenceNumber;
                                        return "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return "Could not obtain string! Debug Message: " + debugMessage;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Nuclex.Fonts;
using DPSF.ParticleSystems;

namespace Spiritus
{
    class GlobalVariables
    {
        // DEBUG VARIABLE
        public static bool DEBUG = false;


        // Core Engine Variables
        public static float displayRatio;
        public static Rectangle rectangleFlashScreen;

        public static SpriteBatch UserInterfaceSpriteBatch;
        public static SpriteBatch ParticleSpriteBatch;
        public static SpriteFont dialogFont;
        public static SpriteFont menuFont;

        public static Texture2D buttonSelected;
        public static Texture2D buttonNotSelected;

        public static VectorFont arialVectorFont;

        // Predefined Draw States
        public static RasterizerState defaultRasterizer;
        public static RasterizerState wireFrame;
        public static BlendState defaultBlendState;
        public static DepthStencilState defaultDepthStencilState;

        // Predefined Values
        public static float alpha100 = 1f;
        public static float alpha0 = 0f;
        public static Vector3 flatPlaneVector = new Plane(
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            new Vector3(0, 0, 1)).Normal;

        // Movement Variables
        public static float globalKeyboardMovementUnit = 0.06f;

        // Battle Variables
        public static Vector3 mainCharacterEncounterPosition;
        public static Matrix mainCharacterEncounterPositionMatrix;
        public static Character encounteredCharacter;

        // Character Variables
        public static Character interactedCharacter;

        public static class CharacterDialog
        {
            public static Dialog dialog = new Dialog();

            public static Dictionary<string, string> DictionaryCharacterCurrentDialog = new Dictionary<string, string>();
            public static Dictionary<string, int> DictionaryCharacterCurrentDialogSequenceNumber = new Dictionary<string, int>();
            public static Dictionary<string, string> DictionaryCharacterNextDialog = new Dictionary<string, string>();
            public static Dictionary<string, string> DictionaryCharacterFirstSequenceDialogs = new Dictionary<string, string>();
        }

        // Character Size in World
        public static float globalWorldCharacterRadius = 0.05f;

        // Fade Variables
        public static double fadeInterval = 5;

        // Flash Screen Variables
        public static bool flashScreenTrigger = false;
        public static float flashScreenAlphaValue = 0;        // To hold the alpha value of the current screen rectangle
        public static Color currentFlashColor;

        // Game State Variables
        public static bool drawWorld = true, drawBattle = false, drawDungeon = false;
        public static bool interacting;

        // User Interface Variables
        public static Vector2 buttonLabelOffset = new Vector2(6, 9);

        // Video Variables
        public static int preferredWindowWidth = 1600;
        public static int preferredWindowHeight = 900;

        // World Variables
        public static int worldHeight;
        public static int worldWidth;
        public static WorldGenerator currentWorld;

        // Defines the boundaries where the main character cannot walk on
        public static Dictionary<string, Plane> worldBoundaries;
        public static bool characterAllowedToMove = true;


        // Particles
        // This list will contain characters + particles
        public interface ListOfSprites { }
        
        public static List<ListOfSprites> listOfWorldSprites = new List<ListOfSprites>();
        public static List<ListOfSprites> listOfBattleSprites = new List<ListOfSprites>();

        // Dummy Particle for comparison
        public static ExtendedParticleEffect dummyParticle = null;
        public static P_IceLance dummyIceLance = null;


        // Randomization Variables
        public static Random enemyTargetRandomizer = new Random();
        public static Random damageRandomizer = new Random();

    }
}

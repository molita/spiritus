﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Spiritus.GUI
{
    public class GUIElement
    {
        protected string name;
        protected Texture2D texture;
        public Vector2 position;
        protected bool visible = true;
        protected byte alpha;
        protected Color alphaColor;
        public Window parent = null;

        public Color getAlphaColor
        {
            get { return alphaColor; }
        }
        public Texture2D getTexture
        {
            get { return texture; }
        }
        public Vector2 getPosition
        {
            get { return position; }
        }

        public string getWindowName
        {
            get { return name; }
        }
        public byte transparency
        {
            set
            {
                alpha = value;
                alphaColor.A = alpha;
            }
            get { return alpha; }
        }
        public bool isVisible
        {
            set { visible = value; }
            get { return visible; }
        }

        public GUIElement(string n, Texture2D t, Vector2 p, GraphicsDevice g)
        {
            name = n;
            texture = t;
            position = p;
            alpha = 255;
            alphaColor = Color.White;
            alphaColor.A = alpha;

        }

        public void DrawElement(GameTime gameTime)
        {
            if (visible == true)
            {
                GlobalVariables.UserInterfaceSpriteBatch.Begin(
                        SpriteSortMode.BackToFront,
                        BlendState.AlphaBlend,
                        SamplerState.AnisotropicWrap,
                        DepthStencilState.DepthRead,
                        GlobalVariables.defaultRasterizer);

                GlobalVariables.UserInterfaceSpriteBatch.Draw(this.getTexture, this.getPosition, this.getAlphaColor);
                GlobalVariables.UserInterfaceSpriteBatch.End();
            }
        }
    }
}

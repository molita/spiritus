﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Spiritus.GUI;

namespace Spiritus.GUI
{
    /// <summary>
    /// This is the Window Object
    /// Windows are comprised of a 2D image.
    /// </summary>
    public class Window : GUIElement
    {
        private List<Window> childrenWindow = null;    // The children windows of this window (if applicable)
        private List<Button> childrenButton = null;    // The children buttons of this window (if applicable)
        private List<Label> childrenLabel = null;      // The children labels of this window (if applicable)

        public Window(string n, Texture2D t, Vector2 p, GraphicsDevice g) : base (n, t, p, g)
        {

        }

        public void addWindow(Window w)
        {
            if (childrenWindow == null)
                childrenWindow = new List<Window>();
            childrenWindow.Add(w);
            w.parent = this;
            w.position = this.position + w.position;
        }

        public void addButton(Button b)
        {
            if (childrenButton == null)
                childrenButton = new List<Button>();
            childrenButton.Add(b);
            b.parent = this;
            b.position = this.position + b.position;
            b.SetButtonLabel();
        }

        public void addLabel(Label l)
        {
            if (childrenLabel == null)
                childrenLabel = new List<Label>();
            childrenLabel.Add(l);
            l.parent = this;
            l.position = this.position + l.position;
        }

        public Button getButton(string name)
        {
            foreach (Button b in childrenButton)
            {
                if (b.getWindowName == name)
                    return b;
            }
            Console.WriteLine("ERROR: COULD NOT FIND BUTTON WITH NAME: " + name);
            return null;
        }

        public void HideWindow()
        {
            if (this.childrenWindow != null)
            {
                foreach (Window w in childrenWindow)
                {
                    w.HideWindow();
                }
            }
            this.isVisible = false;
            if (childrenButton != null)
            {
                foreach (Button b in childrenButton)
                {
                    b.HideButton();
                }
            }
            if (childrenLabel != null)
            {
                foreach (Label l in childrenLabel)
                {
                    l.HideLabel();
                }
            }
        }

        public void drawWindowChildren(GameTime gameTime)
        {
            if (childrenButton != null)
            {
                foreach (Button b in childrenButton)
                {
                    b.DrawElement(gameTime);
                    b.DrawButtonLabel(gameTime);
                }
            }

            if (childrenLabel != null)
            {
                foreach (Label l in childrenLabel)
                {
                    l.DrawLabel(gameTime);
                }
            }

            if (childrenWindow != null)
            {

                foreach (Window w in childrenWindow)
                {
                    w.DrawElement(gameTime);
                }
            }
        }

        public void ShowWindow()
        {
            if (this.childrenWindow != null)
            {
                foreach (Window w in childrenWindow)
                {
                    w.ShowWindow();
                }
            }
            this.isVisible = true;
            if (childrenButton != null)
            {
                foreach (Button b in childrenButton)
                {
                    b.ShowButton();
                }
            }
            if (childrenLabel != null)
            {
                foreach (Label l in childrenLabel)
                {
                    l.ShowLabel();
                }
            }
        }
    }
}

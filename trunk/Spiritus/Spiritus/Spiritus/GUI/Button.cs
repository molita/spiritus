﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Spiritus.GUI
{
    public class Button : GUIElement
    {
        private Button keyUp;
        private Button keyDown;
        private Button keyLeft;
        private Button keyRight;

        private string buttonName;
        private Label buttonLabel;

        private bool buttonSelected;

        public string ButtonName
        {
            set { buttonName = value; }
            get { return buttonName; }
        }

        public Button upButton
        {
            set { keyUp = value; }
            get { return keyUp; }
        }
        public Button downButton
        {
            set { keyDown = value; }
            get { return keyDown; }
        }
        public Button leftButton
        {
            set { keyLeft = value; }
            get { return keyLeft; }
        }
        public Button rightButton
        {
            set { keyRight = value; }
            get { return keyRight; }
        }

        public bool Selected
        {
            set { buttonSelected = value; }
            get { return buttonSelected; }
        }

        public Button(string n, Texture2D t, Vector2 p, GraphicsDevice g, string label) : base (n, t, p, g)
        {
            buttonName = n;
            buttonLabel = new Label(n + "Label", t, p + GlobalVariables.buttonLabelOffset, g, label);
        }

        // This function needs to be run after the button is added to a window
        public void SetButtonLabel()
        {
            buttonLabel.position = this.position + GlobalVariables.buttonLabelOffset;
        }

        public void DrawButtonLabel(GameTime gameTime)
        {
            if (this.buttonSelected == true)
                texture = GlobalVariables.buttonSelected;
            else
                texture = GlobalVariables.buttonNotSelected;
            buttonLabel.DrawLabel(gameTime);
        }

        public void ShowButton()
        {
            this.visible = true;
        }
        public void HideButton()
        {
            this.visible = false;
        }
    }
}

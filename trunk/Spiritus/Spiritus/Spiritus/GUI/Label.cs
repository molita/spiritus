﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Spiritus.GUI;

namespace Spiritus.GUI
{
    public class Label : GUIElement
    {
        private String bodyText;

        public Label(string n, Texture2D t, Vector2 p, GraphicsDevice g, String text) : base (n, t, p, g)
        {
            bodyText = text;
            position = p;
        }

        public void DrawLabel(GameTime gameTime)
        {
            GlobalVariables.UserInterfaceSpriteBatch.Begin(
                SpriteSortMode.BackToFront,
                GlobalVariables.defaultBlendState,
                SamplerState.AnisotropicWrap,
                DepthStencilState.DepthRead,
                GlobalVariables.defaultRasterizer);

            GlobalVariables.UserInterfaceSpriteBatch.DrawString(GlobalVariables.menuFont, bodyText, position, Color.Red);

            GlobalVariables.UserInterfaceSpriteBatch.End();
        }

        public void ShowLabel()
        {
            this.visible = true;
        }
        public void HideLabel()
        {
            this.visible = false;
        }
    }
}

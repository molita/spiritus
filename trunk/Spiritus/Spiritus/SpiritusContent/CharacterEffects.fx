float4x4 World;
float4x4 View;
float4x4 Projection;

// TODO: add effect parameters here.

float3 LightDirection;
float3 LightColour;
float3 AmbientColour;
float Alpha;

texture Shadow;

sampler2D ShadowSampler = sampler_state
{ 
	texture = <Shadow>;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float3 Normal : NORMAL0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float3 Normal : TEXCOORD2;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

	output.UV = input.UV;
	output.Normal = mul(input.Normal, World);
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    // TODO: add your pixel shader code here.

	float4 shadow;

	shadow = tex2D(ShadowSampler, input.UV);

	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDirection);
	float3 lightMod = dot(normal, lightDir);

	lightMod.r = clamp(lightMod.r * LightColour.r, AmbientColour.r, Alpha);
	lightMod.g = clamp(lightMod.g * LightColour.g, AmbientColour.g, Alpha);
	lightMod.b = clamp(lightMod.b * LightColour.b, AmbientColour.b, Alpha);

	float4 output = shadow; 
	output *= float4(lightMod, 1);

    return output;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}

float4x4 World;
float4x4 View;
float4x4 Projection;

// TODO: add effect parameters here.

float4 terrain;
float4 tempPosition; 
float Height;

float3 LightDirection;
float3 LightColour;
float3 AmbientColour;
float Alpha;

texture Water;
texture Mountain;
texture Grass;
texture HeightMap;


sampler2D WaterSampler = sampler_state
{ 
	texture = <Water>;
	Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
sampler2D GrassSampler = sampler_state
{ 
	texture = <Grass>;
	Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
sampler2D MountainSampler = sampler_state
{ 
	texture = <Mountain>;
	Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
sampler2D TestSampler = sampler_state
{ 
	texture = <HeightMap>;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float3 Normal : NORMAL0;
    float2 UV : TEXCOORD0;


    // TODO: add input channels such as texture
    // coordinates and vertex colors here.
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 UV : TEXCOORD0;
	float Height : TEXCOORD1;
	float3 Normal : TEXCOORD2;


    // TODO: add vertex shader outputs such as colors and texture
    // coordinates here. These values will automatically be interpolated
    // over the triangle, and provided as input to your pixel shader.
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

    // TODO: add your vertex shader code here.
	output.UV = input.UV;
	output.Normal = mul(input.Normal, World);
	output.Height = worldPosition[1];
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    // TODO: add your pixel shader code here.

	float test = input.Height;
	float4 terrain;

	if ( test < 5.0f ){
		terrain = tex2D(WaterSampler, input.UV);
	} else if ( test < 20.0f){
		terrain = tex2D(GrassSampler, input.UV);
	} else {
		terrain = tex2D(MountainSampler, input.UV);
	}	

	float4 output = terrain;

	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDirection);
	float3 lightMod = dot(normal, lightDir);

	lightMod.r = clamp(lightMod.r * LightColour.r, AmbientColour.r, Alpha);
	lightMod.g = clamp(lightMod.g * LightColour.g, AmbientColour.g, Alpha);
	lightMod.b = clamp(lightMod.b * LightColour.b, AmbientColour.b, Alpha);
 
	output *= float4(lightMod, Alpha);

    return output;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
